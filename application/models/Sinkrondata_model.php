<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sinkrondata_model extends CI_Model {

	public function data_omset() {
		$tanggal_sekarang = date("Y-m");
		$sql_omset = "
			Declare @Thn_Bln Varchar(7)='2017-12'
			Declare @JumBln Integer=5
			Declare @Usr Varchar(20)='Ervin'

			Declare @Thn Varchar(4)
			Declare @ThnLalu Varchar(4)
			Declare @Thn_Bln_Min Varchar(7)

			Set @Thn=SUBSTRING(@Thn_Bln,1,4)
			Set @ThnLalu=Convert(Int,@Thn)-1

			Declare @Sql Varchar(Max)
			Declare @Tbl_Omzet TABLE (Tahun Varchar(4),Periode Varchar(4),KodeBrg Varchar(6),Omzet Float)

			---- Insert Omzet Tahun Lalu
			If Convert(Int,Substring(@Thn_Bln,6,2))<@JumBln Begin
				Set @Sql='SELECT '+@ThnLalu+',Periode,KodeBrg,SUM(Omzet)
				From(SELECT A.Periode,B.Urut,A.KodeBrg,
						ISNULL((Select SUM(JumUnit) From LINK_POP.Mitra'+Substring(@ThnLalu,3,2)+'.dbo.Mit2JualCus Where Periode=A.Periode and Kode_Prod In (Select Kode_Prod From LINK_POP.Mitra'+Substring(@ThnLalu,3,2)+'.dbo.Mit2Product Where KodeBrg=A.KodeBrg)),0) As Omzet
					From(SELECT B.Periode,A.KodeBrg FROM LINK_POP.Mitra'+Substring(@ThnLalu,3,2)+'.dbo.Mit2Product As A Left Outer Join LINK_POP.Mitra'+Substring(@ThnLalu,3,2)+'.dbo.REFPERIODE3 As B On A.KodeBrg<>B.Periode
						WHERE A.KodeBrg IN (SELECT KodeBrg FROM LINK_POP.Mitra'+Substring(@ThnLalu,3,2)+'.dbo.Mit2Barang WHERE (Type1 = ''Grade'') AND (Type2 = ''Malond'')) and B.Tahun='+@ThnLalu+'
						Group By B.Periode,A.KodeBrg) As A Inner Join LINK_POP.Mitra'+Substring(@ThnLalu,3,2)+'.dbo.Mit2Barang As B On A.KodeBrg=B.KodeBrg) As C
				Group By Periode,Urut,KodeBrg
				Order By Periode,Urut,KodeBrg'
				Insert Into @Tbl_Omzet
				Execute (@Sql)
			End

			---- Insert Omzet Tahun Kini
			Set @Sql='SELECT '+@Thn+',Periode,KodeBrg,SUM(Omzet)
			From(SELECT A.Periode,B.Urut,A.KodeBrg,
					ISNULL((Select SUM(JumUnit) From LINK_POP.Mitra'+Substring(@Thn,3,2)+'.dbo.Mit2JualCus Where Periode=A.Periode and Kode_Prod In (Select Kode_Prod From LINK_POP.Mitra'+Substring(@Thn,3,2)+'.dbo.Mit2Product Where KodeBrg=A.KodeBrg)),0) As Omzet
				From(SELECT B.Periode,A.KodeBrg FROM LINK_POP.Mitra'+Substring(@Thn,3,2)+'.dbo.Mit2Product As A Left Outer Join LINK_POP.Mitra'+Substring(@Thn,3,2)+'.dbo.REFPERIODE3 As B On A.KodeBrg<>B.Periode
					WHERE A.KodeBrg IN (SELECT KodeBrg FROM LINK_POP.Mitra'+Substring(@Thn,3,2)+'.dbo.Mit2Barang WHERE (Type1 = ''Grade'') AND (Type2 = ''Malond'')) and B.Tahun='+@Thn+'
					Group By B.Periode,A.KodeBrg) As A Inner Join LINK_POP.Mitra'+Substring(@Thn,3,2)+'.dbo.Mit2Barang As B On A.KodeBrg=B.KodeBrg) As C
			Group By Periode,Urut,KodeBrg
			Order By Periode,Urut,KodeBrg'
			Insert Into @Tbl_Omzet
			Execute (@Sql)


			Declare @Tbl_Hasil TABLE (Thn_Bln Varchar(7), Tahun Varchar(4), KdBln Varchar(2), NmBln Varchar(15), KodeBrg Varchar(6), Omzet Float)
			Insert Into @Tbl_Hasil
			Select Thn_Bln, Tahun, KdBln, NmBln+' '+Tahun, KodeBrg, SUM(Omzet) As Omzet
			From(Select Distinct A.Tahun+'-'+Substring(A.Bulan,1,2) As Thn_Bln, A.Tahun, Substring(A.Bulan,1,2) As KdBln, Substring(A.Bulan,6,20) As NmBln, A.Periode, B.KodeBrg, B.Omzet
				From LINK_POP.Mitra17.dbo.REFPERIODE3 As A Left Outer Join @Tbl_Omzet As B On A.Periode=B.Periode
				Where A.UserName='Web' and A.Tahun<=@Thn) As A Where Thn_Bln<=@Thn_Bln
			Group By Thn_Bln, Tahun, KdBln, NmBln, KodeBrg
			Order By Thn_Bln

			Select @Thn_Bln_Min=Thn_Bln
			From(Select ROW_NUMBER() OVER (ORDER BY Thn_Bln DESC) AS Urut,Thn_Bln
				From(Select Distinct Thn_Bln from @Tbl_Hasil) As A) As B Where Urut=@JumBln

			Delete from LINK_POP.Mitra17.dbo.Mit2Rekap Where @Usr=@Usr and Jenis1='WebOmzet'
			Insert Into LINK_POP.Mitra17.dbo.Mit2Rekap (UserName,Jenis1,Jenis2,Jenis3,Jenis4,Jenis5,Jumlah1,Type1)
			Select @Usr,'WebOmzet',A.Thn_Bln,A.NmBln,A.KodeBrg,B.NamaBrg,A.Omzet,B.Urut from @Tbl_Hasil As A Inner Join LINK_POP.Mitra17.dbo.Mit2Barang As B On A.KodeBrg=B.KodeBrg
			where A.Thn_Bln>=@Thn_Bln_Min and A.Thn_Bln<=@Thn_Bln

			--Set Pivot ke Kanan
			Declare @deret Varchar(MAX)
			Select @deret =Substring((Select ', ['+A.NmBln+']'  as [text()]  
			From (Select Distinct Tahun,KdBln,NmBln From @Tbl_Hasil where Thn_Bln>=@Thn_Bln_Min and Thn_Bln<=@Thn_Bln) as A
			For Xml Path ('')),2, 2111000)

			Set @Sql='Select * From (Select Type1 As Urut,Jenis3 As Bulan,Jenis4 As KodeBrg,Jenis5 As NamaBrg,Jumlah1 As Omzet from LINK_POP.Mitra17.dbo.Mit2Rekap Where UserName='''+@Usr+''' and Jenis1=''WebOmzet'') As A
			pivot  (Max(Omzet) For Bulan in ('+@Deret+')) as Pvt'
			Execute(@Sql)
		 ";

		 return $this->db->query($sql_omset);
	}

	public function data_stok() {
		$tanggal_sekarang = date("Y-m-d");
		$tahun = date('y');
		$week = date("W", strtotime($tanggal_sekarang));
		$periode = $tahun.''.$week;
		$this->query_laba($periode);

		$sql = "
			Declare @Usr Varchar(20)='StokWeb'
			Declare @Per1 Varchar(4)='$periode'

		-- function backend sendiri load 
		-- Exec LINK_POP.mitra18.dbo.HitLabaMit2 @Usr, 'Los', @Per1, @Per1, 'All-All', 'All'

		Select E.Urut, D.KodeBrg, D.NamaBrg, D.BeratMin, D.BeratMax, D.Pack, Case When D.Pack<>0 Then Ceiling(D.Saldo_Ekor/D.Pack) Else 0 End As Saldo_Pack, D.Saldo_Ekor,@Per1 As Periode
		From(Select A.KodeBrg, C.NamaBrg, B.Nilai1 As BeratMin, B.Nilai2 As BeratMax, B.Nilai3 As Pack, SUM(A.Saldo) As Saldo_Ekor
			From(Select KodeBrg,SUM(JumAkhir) As Saldo From LINK_POP.Mitra18.dbo.AcLabaProd 
					where DbUser=@Usr and Divisi='21' and Periode=@Per1 and Jenis='Zona' and Kode_Pro='002' and Kode_Zona In (Select Kode_Zona From LINK_POP.Mitra18.dbo.RefZona Where Rpb_Prod='1' and Jenis='Zona')
					and KodeBrg In (Select KodeBrg From LINK_POP.Mitra18.dbo.Mit2Barang Where Type1='Grade' and Type2='Malond')
					Group By KodeBrg Having SUM(JumAkhir)<>0
				Union All
				Select KodeBrg,SUM(JumAkhir) As Saldo From LINK_POP.Mitra18.dbo.AcLabaProd 
					where DbUser=@Usr and Divisi='21' and Periode=@Per1 and Jenis='Zona' and Kode_Pro='005' and Kode_Zona In (Select Kode_Zona From LINK_POP.Mitra18.dbo.RefZona Where Rpb_Expe='1' and Jenis='Zona')
					and KodeBrg In (Select KodeBrg From LINK_POP.Mitra18.dbo.Mit2Barang Where Type1='Grade' and Type2='Malond')
					Group By KodeBrg Having SUM(JumAkhir)<>0
				Union All
				Select KodeBrg,SUM(JumAkhir) As Saldo From LINK_POP.Mitra18.dbo.AcLabaProd 
					where DbUser=@Usr and Divisi='21' and Periode=@Per1 and Jenis='Zona' and Kode_Pro='003' and Kode_Zona In (Select Kode_Zona From LINK_POP.Mitra18.dbo.RefZona Where Rpb_Prod='1' and Jenis='Zona')
					and KodeBrg In (Select KodeBrg From LINK_POP.Mitra18.dbo.Mit2Barang Where Type1='Grade' and Type2='Malond')
					Group By KodeBrg Having SUM(JumAkhir)<>0
				Union All
				Select KodeBrg,SUM(JumAkhir) As Saldo From LINK_POP.Mitra18.dbo.AcLabaProd 
					where DbUser=@Usr and Divisi='21' and Periode=@Per1 and Jenis='Depo' and Kode_Pro='003' and Kode_Zona In (Select Kode_Zona From LINK_POP.Mitra18.dbo.RefZona Where Rpb_Prod='1' and Jenis='Depo')
					and KodeBrg In (Select KodeBrg From LINK_POP.Mitra18.dbo.Mit2Barang Where Type1='Grade' and Type2='Malond')
					Group By KodeBrg Having SUM(JumAkhir)<>0) As A Inner Join OpenQuery(LINK_POP, 'select Jenis1, Jenis2, Nilai1, Nilai2, Nilai3 from Mitra18.dbo.Mit2Rule') As B On A.KodeBrg=B.Jenis2 Inner Join LINK_POP.Mitra18.dbo.Mit2Barang As C On A.KodeBrg=C.KodeBrg
			Where B.Jenis1='RangeBerat' and A.KodeBrg <> 'PM0004'
			Group By A.KodeBrg,C.NamaBrg,B.Nilai1,B.Nilai2,B.Nilai3) As D Inner Join LINK_POP.Mitra18.dbo.Mit2Barang As E On D.KodeBrg=E.KodeBrg
			Order By E.Urut
		 ";

		return $this->db->query($sql);
	}

	public function query_laba($periode) {
		$sql = "
				Declare @Usr Varchar(20)='StokWeb'
				Declare @Per1 Varchar(4)='$periode'
		
				-- function backend sendiri load 
				Exec LINK_POP.mitra18.dbo.HitLabaMit2 @Usr, 'Los', @Per1, @Per1, 'All-All', 'All'
		 ";
		 $this->db->query($sql);
	}



}

/* End of file Sinkrondata_model.php */
/* Location: ./application/models/Sinkrondata_model.php */