<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni_model extends CI_Model {

	public function view_all_testimoni($table) {
		$this->db->select('*');
		return $this->db->get($table);
	}
	
	public function insert_data_testimoni($table, $data) {
		$this->db->insert($table, $data);
	}

	public function update_data_testimoni($table, $key_code, $where) {
		$this->db->select('*');
		$this->db->where('kode_testimoni', $key_code);
		$this->db->update($table, $where);
	}

	public function delete_data_testimoni($table, $key_code) {
		$this->db->select('*');
		$this->db->where('kode_kerjasama', $key_code);
		$this->db->delete($table);
	}

	/**
	
		TODO:
		- Model Testimoni Video
		- Testimoni Persolan + Vidoe
	
	 */
	
	// public function view_all_testimoni_video() {
	// 	$this->db->select('*');
	// 	return $this->db->get('tbl_malond_testimoni_video');
	// }
	
	// public function view_data_by_kode_testimoni_video($table, $kode_testimoni) {
	// 	$this->db->where('kode_testimoni_video', $kode_testimoni);
	// 	return $this->db->get($table, 1);
	// }

	// public function insert_data_testimoni_video($table, $data) {
	// 	$this->db->insert($table, $data);
	// }

	// public function update_data_testimoni_video($table, $kode_testimoni, $where_data) {
	// 	$this->db->where('kode_testimoni_video', $kode_testimoni);
	// 	$this->db->update($table, $where_data);
	// }

	// public function delete_data_testimoni_video($table, $kode_testimoni) {
	// 	$this->db->where('kode_testimoni_video', $kode_testimoni);
	// 	$this->db->delete($table);
	// }

}

/* End of file Testimoni_model.php */
/* Location: ./application/models/Testimoni_model.php */