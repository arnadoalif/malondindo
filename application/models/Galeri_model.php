<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeri_model extends CI_Model {

	public function view_all_galeri($table) {
		$this->db->select('*');
		return $this->db->get($table);
	}
	
	public function insert_data_galeri($table, $data) {
		$this->db->insert($table, $data);
	}

	public function update_data_galeri($table, $key_code, $where) {
		$this->db->select('*');
		$this->db->where('kode_galeri', $key_code);
		$this->db->update($table, $where);
	}

	public function delete_data_galeri($table, $key_code) {
		$this->db->select('*');
		$this->db->where('kode_galeri', $key_code);
		$this->db->delete($table);
	}

	public function view_galeri_kategori() {
		$sql = "

			SELECT TOP (200) tbl_malond_galeri.kode_galeri, tbl_malond_galeri.kode_kategori_galeri, tbl_malond_galeri.nama_galeri, tbl_malond_galeri.nama_file_galeri, tbl_malond_galeri.create_at, tbl_malond_kategori.nama_kategori,tbl_malond_galeri.link_galeri,
			tbl_malond_kategori.key_class_kategori
			FROM tbl_malond_galeri INNER JOIN
			tbl_malond_kategori ON tbl_malond_galeri.kode_kategori_galeri = tbl_malond_kategori.kode_kategori_galeri order by kode_galeri
		 ";

		return $this->db->query($sql);
	}
	

}

/* End of file Galeri_model.php */
/* Location: ./application/models/Galeri_model.php */