<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_model extends CI_Model {

	public function view_data_sales() {
		$this->db->select('*');
		return $this->db->get('tbl_malond_sales');
	}

	public function insert_data_sales($table, $data) {
		$this->db->insert($table, $data);
	}	
	
	public function update_data_sales($table, $kode_key, $where) {
		$this->db->select('*');
		$this->db->where('kode_sales', $kode_key);
		$this->db->update($table, $where);
	}

	public function delete_data_sales($table, $kode_key, $where) {
		$this->db->select('*');
		$this->db->where('kode_sales', $kode_sales);
		$this->db->delete($table);
	}

}

/* End of file Sales_model.php */
/* Location: ./application/models/Sales_model.php */