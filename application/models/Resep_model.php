<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resep_model extends CI_Model {

	public function view_data_resep() {
		$this->db->select('*');
		return $this->db->get('tbl_malond_resep');
	}

	public function view_data_by_slug($table, $slug_resep) {
		$this->db->where('slug_resep', $slug_resep);
		return $this->db->get($table, 1);
	}	

	public function view_data_by_kode_resep($table, $kode_resep) {
		$this->db->where('kode_resep', $kode_resep);
		return $this->db->get($table, 1);
	}

	public function insert_data_resep($table, $data) {
		$this->db->insert($table, $data);
	}

	public function update_data_resep($table, $kode_resep, $where) {
		$this->db->where('kode_resep', $kode_resep);
		$this->db->update($table, $where);
	}

	public function delete_data_resep($table, $link_id_resep_youtube) {
		$this->db->where('link_id_resep_youtube', $link_id_resep_youtube);
		$this->db->delete($table);
	}

}

/* End of file Resep_model.php */
/* Location: ./application/models/Resep_model.php */