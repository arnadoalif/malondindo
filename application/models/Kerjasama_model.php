<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kerjasama_model extends CI_Model {

	public function view_all_kerjasama($table) {
		$this->db->select('*');
		return $this->db->get($table);
	}
	
	public function insert_data_kerjasama($table, $data) {
		$this->db->insert($table, $data);
	}

	public function update_data_kerjasama($table, $key_code, $where) {
		$this->db->select('*');
		$this->db->where('kode_kerjasama', $key_code);
		$this->db->update($table, $where);
	}

	public function delete_data_kerjasama($table, $key_code) {
		$this->db->select('*');
		$this->db->where('kode_kerjasama', $key_code);
		$this->db->delete($table);
	}

}

/* End of file Kerjasama_model.php */
/* Location: ./application/models/Kerjasama_model.php */