<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorigaleri_model extends CI_Model {

	public function view_categori_galeri_all($table) {
		$this->db->select('*');
		return $this->db->get($table);
	}	

	public function insert_data_categori_galeri($table, $data) {
		$this->db->insert($table, $data);
	}

	public function update_data_categori_galeri($table, $kode_key, $where) {
		$this->db->where('kode_kategori_galeri', $kode_key);
		$this->db->update($table, $where);
	}

	public function delete_data_categori_galeri($table, $kode_key) {
		$this->db->where('kode_kategori_galeri', $kode_key);
		$this->db->delete($table);
	}

}

/* End of file Categorigaleri_model.php */
/* Location: ./application/models/Categorigaleri_model.php */