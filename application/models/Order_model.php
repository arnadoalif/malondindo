<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

	public function view_data_order() {
		$this->db->select('*');
		return $this->db->get('tbl_malond_order');
	}

	public function count_primary($table) {
		$this->db->select('kode_order');
		$this->db->order_by('kode_order', 'desc');
		return $this->db->get($table, 1)->first_row();
	}

	public function insert_data_order($table, $data) {
		$this->db->insert($table, $data);
	}

	public function buat_kode_order($nomor_terakhir, $kunci, $jumlah_karakter = 0)
	{
	    /* mencari nomor baru dengan memecah nomor terakhir dan menambahkan 1
	    string nomor baru dibawah ini harus dengan format XXX000000 
	    untuk penggunaan dalam format lain anda harus menyesuaikan sendiri */
	    $nomor_baru = intval(substr($nomor_terakhir, strlen($kunci))) + 1;
	//    menambahkan nol didepan nomor baru sesuai panjang jumlah karakter
	    $nomor_baru_plus_nol = str_pad($nomor_baru, $jumlah_karakter, "0", STR_PAD_LEFT);
	//    menyusun kunci dan nomor baru
	    $kode = $kunci . $nomor_baru_plus_nol;
	    return $kode;
	}

}

/* End of file Order_model.php */
/* Location: ./application/models/Order_model.php */