<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimokostumer_model extends CI_Model {

	public function view_all_kostumer_testimoni() {
		$this->db->select('*');
		return $this->db->get('tbl_malond_testimoni_kostumer');
	}

	public function insert_kostumer_testimoni($table_name, $data) {
		print_r($data);
		$this->db->insert($table_name, $data);
	}

	public function update_kostumer_testimoni($table_name, $kode_key, $where) {
		$this->db->where('kode_testimon_kostumer', $kode_key);
		$this->db->update($table_name, $where);
	}

	public function delete_kostumer_testimoni($table_name) {
		$this->db->where('kode_testimon_kostumer', $kode_key);
		$this->db->delete($table_name);
	}

}

/* End of file Testimokostumer_model.php */
/* Location: ./application/models/Testimokostumer_model.php */