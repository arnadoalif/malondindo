<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function update_status_login($table, $username, $password, $condition) {
		
		if ($condition == "login") {
			$data = array ('status_login' => '1', 'login_time' => date("Y-m-d H:i:s"));
			$this->db->select('*');
			$this->db->where('username', $username);
			$this->db->where('password', $password);
			$this->db->update($table, $data);
		} else {
			$data = array ('status_login' => '0', 'logout_time' => date("Y-m-d H:i:s"));
			$this->db->select('*');
			$this->db->where('username', $username);
			$this->db->where('password', $password);
			$this->db->update($table, $data);
		}
		
	}	

}

/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */