<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inbox_model extends CI_Model {

	public function view_all_inbox($table) {
		$this->db->select('*');
		return $this->db->get($table);
	}
	
	public function insert_data_inbox($table, $data) {
		$this->db->insert($table, $data);
	}

	public function update_data_inbox($table, $key_code, $where) {
		$this->db->select('*');
		$this->db->where('kode_inbox', $key_code);
		$this->db->update($table, $where);
	}

	public function delete_data_inbox($table, $key_code) {
		$this->db->select('*');
		$this->db->where('kode_inbox', $key_code);
		$this->db->delete($table);
	}

}

/* End of file Inbox_model.php */
/* Location: ./application/models/Inbox_model.php */