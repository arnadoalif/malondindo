<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontpage extends CI_Controller {

	public function index()
	{
		$this->load->model('Testimoni_model');
		$this->load->model('Kerjasama_model');

		$m_testimoni = new Testimoni_model();
		$m_kerjasama = new Kerjasama_model();

		$data['data_testimoni'] = $m_testimoni->view_all_testimoni('tbl_malond_testimoni')->result();
		$data['data_kerjasama'] = $m_kerjasama->view_all_kerjasama('tbl_malond_kerjasama')->result();

		$this->load->view('front_pages/page/home_view', $data);
	}

	public function profile() {
		$this->load->model('Kerjasama_model');
		$m_kerjasama = new Kerjasama_model();
		$data['data_kerjasama'] = $m_kerjasama->view_all_kerjasama('tbl_malond_kerjasama')->result();

		$this->load->view('front_pages/page/profile_view', $data);
	}

	public function produk() {
		$this->load->model('Kerjasama_model');
		$m_kerjasama = new Kerjasama_model();
		$data['data_kerjasama'] = $m_kerjasama->view_all_kerjasama('tbl_malond_kerjasama')->result();
		
		$this->load->view('front_pages/page/produk_view', $data);
	}

	public function testimoni() {
		$this->load->view('front_pages/page/testimoni_view');
	}

	public function resep() {
		$this->load->model('Resep_model');
		$m_resep = new Resep_model();

		$data['data_resep'] = $m_resep->view_data_resep()->result();
		$this->load->view('front_pages/page/resep_view', $data);
	}

	public function detail_resep($slug_resep) {
		$this->load->model('Resep_model');
		$m_resep = new Resep_model();

		$data['detail_resep'] = $m_resep->view_data_by_slug('tbl_malond_resep', $slug_resep)->result();
		$this->load->view('front_pages/page/detail_resep_view', $data);
	}

	public function kontak() {
		$this->load->view('front_pages/page/kontak_view');
	}

	public function action_inbox() {
		$this->load->model('Inbox_model');
		$m_inbox = new Inbox_model();

		$nama_inbox 	= htmlspecialchars($this->input->post('nama_inbox'));
		$nomor_telepon  = htmlspecialchars($this->input->post('nomor_telepon'));
		$email_inbox 	= htmlspecialchars($this->input->post('email_inbox'));
		$isi_inbox   	= htmlspecialchars($this->input->post('isi_inbox'));

		if (empty($nama_inbox) && empty($nomor_telepon) && empty($email_inbox) && empty($isi_inbox)) {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! gagal disimpan </strong> Form tidak boleh kosong.');
			redirect($this->agent->referrer());

		} else {

			$data_inbox = array(
				'nama_inbox' 	=> $nama_inbox,
				'nomor_telepon' => $nomor_telepon,
				'email_inbox' 	=> $email_inbox,
				'isi_inbox' 	=> $isi_inbox,
				'create_at' 	=> date("Y-m-d H:i:s")
			);

			$m_inbox->insert_data_inbox('tbl_malond_pesan', $data_inbox);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Pesan anda berhasil disimpan.');
			redirect($this->agent->referrer());

		}
	}
}

/* End of file Frontpage.php */
/* Location: ./application/controllers/Frontpage.php */