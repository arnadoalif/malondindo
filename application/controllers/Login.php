<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		$this->load->view('login_system/login_system_view');
	}

	public function page_notfound() {
		$this->load->view('404_view');
	}

	public function action_login() {
		$this->load->model('Login_model');
		$m_login = new Login_model();

		$username = htmlspecialchars($this->input->post('user_name'));
		$password = md5(htmlspecialchars($this->input->post('password')));
		$sql = "SELECT * FROM tbl_malond_login WHERE username = '$username' AND password = '$password'";
		$valid = $this->db->query($sql)->num_rows();
		if ($valid > 0) {
			$data_admin = $this->db->query($sql)->row();
			$data_session = array(
					'full_name' => $data_admin->full_name,
					'username' => $data_admin->username,
					'password' => $data_admin->password,
					'status_login' => 'login'
				);
			$this->session->set_userdata($data_session);
			$m_login->update_status_login('tbl_malond_login',$username,$password,'login');
			redirect('admin','refresh');
		} else {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Login gagal');
			redirect($this->agent->referrer());
		}
	}

	public function logout() {
		$this->load->model('Login_model');
		$m_login = new Login_model();
		$username = $this->session->userdata('username');
		$password = $this->session->userdata('password');
		$m_login->update_status_login('tbl_malond_login', $username, $password, 'logout');
		$this->session->unset_userdata(array('full_name' => '', 'username' => '', 'password' => '', 'status_login' => ''));
		session_destroy();
		redirect(base_url());
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */