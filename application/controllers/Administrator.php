<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		
		if($this->session->userdata('status_login') != "login"){
			redirect('login');
		}

		$this->load->library('user_agent');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index() {
		$tanggal_sekarang = date("Y-m-d");
		$tahun = date('y');
		$week = date("W", strtotime($tanggal_sekarang));
		$periode = $tahun.''.$week;

		$this->load->model('Sinkrondata_model');
		$m_sinkrondata = new Sinkrondata_model();

		$data['periode'] = $periode;
		$data['data_stok_malond'] = $m_sinkrondata->data_stok()->result();
		$this->load->view('administrator/page/home_default_view', $data);
	}

	public function view_order() {
		$this->load->model('Order_model');
		$m_order = new Order_model();

		$data['data_order'] = $m_order->view_data_order()->result();
		$this->load->view('administrator/page/data_order_view', $data);
	}

	public function view_produk() {
		$tanggal_sekarang = date("Y-m-d");
		$tahun = date('y');
		$week = date("W", strtotime($tanggal_sekarang));
		$periode = $tahun.''.$week;

		$this->load->model('Sinkrondata_model');
		$m_sinkrondata = new Sinkrondata_model();
		$data['periode'] = $periode;
		$data['data_stok_malond'] = $m_sinkrondata->data_stok()->result();
		$this->load->view('administrator/page/data_produk_view', $data);
	}

	public function view_galeri() {
		$this->load->model('Categorigaleri_model');
		$this->load->model('Galeri_model');
		$m_galeri = new Galeri_model();
		$m_categori = new Categorigaleri_model();

		$data['data_galeri'] = $m_galeri->view_galeri_kategori()->result();
		$data['data_categori'] = $m_categori->view_categori_galeri_all('tbl_malond_kategori')->result();
		$this->load->view('administrator/page/data_galeri_view', $data);
	}

	public function view_testimoni() {
		$this->load->view('administrator/page/testimoni_view');
	}

	public function view_testimoni_personal() {
		$this->load->model('Testimoni_model');
		$m_testimoni = new Testimoni_model();

		$data['data_testimoni'] = $m_testimoni->view_all_testimoni('tbl_malond_testimoni')->result();
		$this->load->view('administrator/page/data_testimoni_view', $data);
	}

	public function view_kerjasama() {
		$this->load->model('Kerjasama_model');
		$m_kerjasama = new Kerjasama_model();

		$data['data_kerjasama'] = $m_kerjasama->view_all_kerjasama('tbl_malond_kerjasama')->result();
		$this->load->view('administrator/page/data_kerjasama_view', $data);
	}

	public function view_inbox() {
		$this->load->model('Inbox_model');
		$m_inbox = new Inbox_model();

		$data['data_inbox'] = $m_inbox->view_all_inbox("tbl_malond_pesan")->result();
		$this->load->view('administrator/page/data_inbox_view', $data);
	}

	public function view_setting_sales() {
		$this->load->model('Sales_model');
		$m_sales = new Sales_model();

		$data['data_sales'] = $m_sales->view_data_sales()->result();
		$this->load->view('administrator/page/data_setting_sales_view', $data);
	}

	public function view_testimoni_kostumer() {
		$this->load->model('Testimokostumer_model');
		$m_testimonkostumer = new Testimokostumer_model();

		$data['data_testimoni_kostumer'] = $m_testimonkostumer->view_all_kostumer_testimoni()->result();
		$this->load->view('administrator/page/data_testimoni_kostumer_view', $data);
	}

	public function view_resep() {
		$this->load->model('Resep_model');
		$m_resep = new Resep_model();

		$data['data_resep'] = $m_resep->view_data_resep()->result();
		$this->load->view('administrator/page/data_resep_view', $data);
	}


	public function view_edit_resep($kode_resep) {
		$this->load->model('Resep_model');
		$m_resep = new Resep_model();


		$valid = $this->db->query("SELECT * FROM tbl_malond_resep WHERE kode_resep = '$kode_resep'")->num_rows();
		if ($valid > 0) {
			$data['detail_resep'] = $m_resep->view_data_by_kode_resep("tbl_malond_resep", $kode_resep)->result();
			$this->load->view('administrator/page/edit_data_resep', $data);
		} else {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Kode resep ini tidak tersedia.');
			redirect($this->agent->referrer());
		}
	}

	public function view_testimoni_video() {
		$this->load->view('administrator/page/data_testimoni_video_view');
	}


	public function input_data_produk() {
		$this->load->view('administrator/page/input_data_produk_view');
	}

	public function input_data_galeri() {
		$this->load->model('Categorigaleri_model');
		$m_categori = new Categorigaleri_model();
		$data['data_categori'] = $m_categori->view_categori_galeri_all('tbl_malond_kategori')->result();

		$this->load->view('administrator/page/input_data_galeri_view', $data);
	}

	public function input_data_testimoni() {
		$this->load->view('administrator/page/input_data_testimoni_view');
	}

	public function input_data_kerjasama() {
		$this->load->view('administrator/page/input_data_kerjasama_view');
	}

	public function input_data_categori() {
		$this->load->model('Categorigaleri_model');
		$m_categori = new Categorigaleri_model();
		$data['data_categori'] = $m_categori->view_categori_galeri_all('tbl_malond_kategori')->result();

		$this->load->view('administrator/page/input_data_tambah_categori_view', $data);
	}

	public function test() {
		$this->load->model('Sinkrondata_model');
		$m_sinkrondata = new Sinkrondata_model();
		print_r($m_sinkrondata->data_stok()->result());
	}

	public function input_data_resep() {
		$this->load->view('administrator/page/input_data_resep_view');
	}

	/**
	
		TODO:
		- system engine
		- work backend alif benden arnado
	 */
	

	public function action_input_resep() {
		$this->load->model('Resep_model');
		$m_resep = new Resep_model();

		$nama_resep = htmlspecialchars($this->input->post('nama_resep'));
		$kode_link  = htmlspecialchars($this->input->post('kode_link'));
		$keterangan_resep = $this->input->post('keterangan_resep');
		$remove_strip = str_replace("-"," ", $nama_resep);
		$slug_url = $this->slug($remove_strip);

		$valid = $this->db->query("SELECT * FROM tbl_malond_resep WHERE slug_resep = '$slug_url' AND nama_resep = '$nama_resep'")->num_rows();

		if ($valid > 0) {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Nama resep ini sudah tersedia.');
			redirect($this->agent->referrer());
		} else {
			$data_insert =  array(
								'slug_resep' => $slug_url,
								'nama_resep' => $nama_resep,
								'link_id_resep_youtube' => $kode_link,
								'deskripsi_resep' => $keterangan_resep,
								'tanggal_input' => date("Y-m-d H:i:s")
							);
			$m_resep->insert_data_resep('tbl_malond_resep', $data_insert);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Resep berhasil di tambahkan.');
			redirect('admin/resep','refresh');
		}
	}

	public function action_edit_resep() {
		$this->load->model('Resep_model');
		$m_resep = new Resep_model();

		$kode_resep = htmlspecialchars($this->input->post('kode_resep'));
		$nama_resep = htmlspecialchars($this->input->post('nama_resep'));
		$kode_link  = htmlspecialchars($this->input->post('kode_link'));
		$keterangan_resep = $this->input->post('keterangan_resep');
		$remove_strip = str_replace("-"," ", $nama_resep);
		$slug_url = $this->slug($remove_strip);

		$valid = $this->db->query("SELECT * FROM tbl_malond_resep WHERE kode_resep = '$kode_resep'")->num_rows();
		if ($valid > 0) {
			$data_update = array(
				'slug_resep' => $slug_url,
				'nama_resep' => $nama_resep,
				'link_id_resep_youtube' => $kode_link,
				'deskripsi_resep' => $keterangan_resep
			);

			$m_resep->update_data_resep('tbl_malond_resep', $kode_resep, $data_update);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Resep berhasil di ubah.');
			redirect('admin/resep','refresh');
		} else {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong>Kode ini tidak tersedia.');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete_resep($kode_id_youtube) {
		$this->load->model('Resep_model');
		$m_resep = new Resep_model();

		$valid = $this->db->query("SELECT * FROM tbl_malond_resep WHERE link_id_resep_youtube = '$kode_id_youtube'")->num_rows();
		if ($valid > 0) {
			$m_resep->delete_data_resep('tbl_malond_resep', $kode_id_youtube);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Resep berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Nama resep ini sudah tersedia.');
			redirect($this->agent->referrer());
		}
	}
	
	public function action_input_galeri() {
		
		$this->load->model('Galeri_model');
		$m_galeri = new Galeri_model();

		$nama_galeri     = htmlspecialchars($this->input->post('nama_galeri'));
		$kategori_galeri = htmlspecialchars($this->input->post('kategori_galeri'));
		$link_galeri     = htmlspecialchars($this->input->post('link_galeri')); 
		$file_name       = $_FILES['logo_galeri']['name'];
		$error 			 = $_FILES['logo_galeri']['error'];
		$remove_strip_nama_gambar 	= str_replace("-"," ", $nama_galeri);
		$nama_gambar_slug   		= $this->slug($remove_strip_nama_gambar);
		$nama_gambar   				= str_replace("-","_", $nama_gambar_slug);


		if ($error > 0) {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Galeri gagal di tambahkan file tidak ada.');
			redirect($this->agent->referrer());
		} else {
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = date('Y_d_s').'_'.$nama_gambar.'.'.$file_type; 

			$config['upload_path'] = './assets_default/img_galeri/';
			$config['allowed_types'] = '*';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('logo_galeri')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Galeri gagal di tambahkan.');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}
		}

		$data_galeri = array(
							'kode_kategori_galeri' => $kategori_galeri,
							'nama_galeri' => $nama_galeri,
							'nama_file_galeri' => $new_file_name,
							'link_galeri' => $link_galeri,
							'create_at' => date("Y-m-d H:i:s")
						);
		$m_galeri->insert_data_galeri('tbl_malond_galeri',$data_galeri);
		$this->session->set_flashdata('message_data', '<strong>Success</strong> Galeri berhasil di tambahkan.');
		redirect($this->agent->referrer());
	}

	public function action_update_galeri() {
		$this->load->model('Galeri_model');
		$m_galeri = new Galeri_model();

		$kode_galeri     = htmlspecialchars($this->input->post('kode_galeri'));
		$nama_galeri 	 = htmlspecialchars($this->input->post('nama_galeri'));
		$kategori_galeri = htmlspecialchars($this->input->post('kategori_galeri'));
		$file_name    	 = $_FILES['logo_galeri']['name'];
		$error 			 = $_FILES['logo_galeri']['error'];
		$remove_strip_nama_gambar = str_replace("-"," ", $nama_galeri);
		$nama_gambar_slug   = $this->slug($remove_strip_nama_gambar);
		$nama_gambar   		= str_replace("-","_", $nama_gambar_slug);
		$img_old         = htmlspecialchars($this->input->post('img_old'));
		$link_galeri     = htmlspecialchars($this->input->post('link_galeri'));

		$valid = $this->db->query("SELECT * FROM tbl_malond_galeri WHERE kode_galeri = '$kode_galeri'")->num_rows();
		if ($valid > 0) {

			if ($_FILES['logo_galeri']['tmp_name'] == '') {
				
				$data_galeri = array(
							'kode_kategori_galeri' 	=> $kategori_galeri,
							'nama_galeri' 		   	=> $nama_galeri,
							'link_galeri'			=> $link_galeri,
							'nama_file_galeri' 		=> $img_old
						);
				$m_galeri->update_data_galeri('tbl_malond_galeri',$kode_galeri, $data_galeri);
				$this->session->set_flashdata('message_data', '<strong>Success</strong> Galeri berhasil di ubah.');
				redirect($this->agent->referrer());

			} else {

				$dir_remove_file_old = "./assets_default/img_galeri/".$this->input->post('img_old');
				if (file_exists($dir_remove_file_old)) {
					unlink($dir_remove_file_old);
				}

				$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
				$new_file_name = date('Y_d_s').'_'.$nama_gambar.'.'.$file_type; 

				$config['upload_path'] = './assets_default/img_galeri/';
				$config['allowed_types'] = '*';
				$config['max_size']  = '*';
				$config['file_name'] = $new_file_name;

				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('logo_galeri')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Galeri gagal di tambahkan.');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}
				
				$data_galeri = array(
								'kode_kategori_galeri' => $kategori_galeri,
								'nama_galeri' => $nama_galeri,
								'link_galeri'	=> $link_galeri,
								'nama_file_galeri' => $new_file_name,
							);
				$m_galeri->update_data_galeri('tbl_malond_galeri',$kode_galeri, $data_galeri);
				$this->session->set_flashdata('message_data', '<strong>Success</strong> Galeri berhasil di ubah.');
				redirect($this->agent->referrer());

			}
		} else {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Galeri gagal di ubah.');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete_galeri($kode_galeri) {
		$this->load->model('Galeri_model');
		$m_galeri = new Galeri_model();
		$valid = $this->db->query("SELECT * FROM tbl_malond_galeri WHERE kode_galeri = '$kode_galeri'")->num_rows();
		$get_file_name	= $this->db->query("SELECT * FROM tbl_malond_galeri WHERE kode_galeri = '$kode_galeri'")->row();
		if ($valid > 0) {
			echo 'data ada';
			$dir_remove_file_old = "./assets_default/img_galeri/".$get_file_name->nama_file_galeri;
			if (file_exists($dir_remove_file_old)) {
				unlink($dir_remove_file_old);
			}
			$m_galeri->delete_data_galeri('tbl_malond_galeri', $kode_galeri);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Galeri berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Galeri gagal di hapus.');
			redirect($this->agent->referrer());
		}
	}

	public function action_input_categori_galeri() {
		$this->load->model('Categorigaleri_model');
		$m_categori_model = new Categorigaleri_model();

		$nama_kategori 		  = htmlspecialchars($this->input->post('nama_kategori'));
		$nama_kategori_slug   = $this->slug($nama_kategori);
		$key_class_kategori   = str_replace("-","_", $nama_kategori_slug);

		$data_kategori = array('nama_kategori' => $nama_kategori, 'key_class_kategori' => $key_class_kategori);
		$m_categori_model->insert_data_categori_galeri('tbl_malond_kategori', $data_kategori);
		redirect($this->agent->referrer());
	}

	public function action_update_categori_galeri() {
		$this->load->model('Categorigaleri_model');
		$m_categori_model = new Categorigaleri_model();

		$kode_kategori_galeri = $this->input->post('kode_kategori_galeri');
		$nama_kategori 		  = $this->input->post('nama_kategori');
		$nama_kategori_slug   = $this->slug($nama_kategori);
		$key_class_kategori   = str_replace("-","_", $nama_kategori_slug);

		$valid = $this->db->query("SELECT * FROM tbl_malond_kategori WHERE kode_kategori_galeri = '$kode_kategori_galeri'")->num_rows();
		if ($valid > 0) {
			$where = array(
				'nama_kategori' => $nama_kategori,
				'key_class_kategori' => $key_class_kategori
			);
			$m_categori_model->update_data_categori_galeri('tbl_malond_kategori', $kode_kategori_galeri, $where);
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Kategori galeri gagal di ubah.');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete_categori_galeri($kode_kategori_galeri) {
		$this->load->model('Categorigaleri_model');
		$m_categori_model = new Categorigaleri_model();

		$valid = $this->db->query("SELECT * FROM tbl_malond_kategori WHERE kode_kategori_galeri = '$kode_kategori_galeri'")->num_rows();
		if ($valid > 0) {
			$m_categori_model->delete_data_categori_galeri('tbl_malond_kategori', $kode_kategori_galeri);
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Kategori galeri gagal di hapus.');
			redirect($this->agent->referrer());
		}
	}

	public function action_input_testimoni() {
		$this->load->model('Testimoni_model');
		$m_testimoni = new Testimoni_model();

		$nama_testimoni 	= htmlspecialchars($this->input->post('nama_testimoni'));
		$alamat_testimoni 	= htmlspecialchars($this->input->post('alamat_testimoni'));
		$pesan_testimoni 	= htmlspecialchars($this->input->post('pesan_testimoni'));

		$file_name    		= $_FILES['logo_testimoni']['name'];
		$error 				= $_FILES['logo_testimoni']['error'];
		$remove_strip_nama_gambar = str_replace("-"," ", $nama_testimoni);
		$nama_gambar_slug   = $this->slug($remove_strip_nama_gambar);
		$nama_gambar   		= str_replace("-","_", $nama_gambar_slug);

		if ($error > 0) {
			$data_testimoni = array(
				'nama_testimoni' 	=> $nama_testimoni,
				'alamat_testimoni' 	=> $alamat_testimoni,
				'pesan_testimoni' 	=> $pesan_testimoni,
				'nama_file' 		=> 'default.png',
				'create_at' 		=> date("Y-m-d H:i:s")
			);
			$m_testimoni->insert_data_testimoni('tbl_malond_testimoni', $data_testimoni);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Testimoni berhasil di tambahkan.');
			redirect($this->agent->referrer());
		} else {
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = date('Y_d_s').'_'.$nama_gambar.'.'.$file_type; 

			$config['upload_path'] = './assets_default/img_testimoni/';
			$config['allowed_types'] = '*';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('logo_testimoni')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Testimoni gagal di tambahkan.');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}
		}

		$data_testimoni = array(
			'nama_testimoni' 	=> $nama_testimoni,
			'alamat_testimoni' 	=> $alamat_testimoni,
			'pesan_testimoni' 	=> $pesan_testimoni,
			'nama_file' 		=> $new_file_name,
			'create_at' 		=> date("Y-m-d H:i:s")
		);
		$m_testimoni->insert_data_testimoni('tbl_malond_testimoni', $data_testimoni);
		$this->session->set_flashdata('message_data', '<strong>Success</strong> Testimoni berhasil di tambahkan.');
		redirect($this->agent->referrer());
	}

	public function action_update_testimoni() {
		$this->load->model('Testimoni_model');
		$m_testimoni = new Testimoni_model();
		$kode_testimoni 	= htmlspecialchars($this->input->post('kode_testimoni'));
		$nama_testimoni 	= htmlspecialchars($this->input->post('nama_testimoni'));
		$alamat_testimoni 	= htmlspecialchars($this->input->post('alamat_testimoni'));
		$pesan_testimoni 	= htmlspecialchars($this->input->post('pesan_testimoni'));
		$img_old            = htmlspecialchars($this->input->post('img_old'));

		$valid = $this->db->query("SELECT * FROM tbl_malond_testimoni WHERE kode_testimoni = '$kode_testimoni'")->num_rows();
		if ($valid > 0) {

			if ($_FILES['logo_testimoni']['tmp_name'] = '') {
				$data_testimoni = array(
					'nama_testimoni' => $nama_testimoni,
					'alamat_testimoni' => $alamat_testimoni,
					'pesan_testimoni' => $pesan_testimoni,
					'nama_file'		=> $img_old
				);

				$m_testimoni->update_data_testimoni('tbl_malond_testimoni', $kode_testimoni , $data_testimoni);
				$this->session->set_flashdata('message_data', '<strong>Success</strong> Testimoni berhasil di di ubah.');
				redirect($this->agent->referrer());
			} else {

				$dir_remove_file_old = "./assets_default/img_testimoni/".$this->input->post('img_old');
				print_r($dir_remove_file_old);
				if (file_exists($dir_remove_file_old)) {
					unlink($dir_remove_file_old);
				}

				$file_name    				= $_FILES['logo_testimoni']['name'];
				$error 						= $_FILES['logo_testimoni']['error'];
				$remove_strip_nama_gambar 	= str_replace("-"," ", $nama_testimoni);
				$nama_gambar_slug   		= $this->slug($remove_strip_nama_gambar);
				$nama_gambar   				= str_replace("-","_", $nama_gambar_slug);

				$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
				$new_file_name = date('Y_d_s').'_'.$nama_gambar.'.'.$file_type; 

				$config['upload_path'] = './assets_default/img_testimoni/';
				$config['allowed_types'] = '*';
				$config['max_size']  = '*';
				$config['file_name'] = $new_file_name;
				
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('logo_testimoni')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Testimoni gagal di ubah.');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}
			}

			$data_testimoni = array(
				'nama_testimoni' => $nama_testimoni,
				'alamat_testimoni' => $alamat_testimoni,
				'pesan_testimoni' => $pesan_testimoni,
				'nama_file'		=> $new_file_name
			);

			$m_testimoni->update_data_testimoni('tbl_malond_testimoni', $kode_testimoni , $data_testimoni);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Testimoni berhasil di ubah.');
			redirect($this->agent->referrer());

		} else {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Testimoni gagal di ubah.');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete_testimoni($key_code) {
		$this->load->model('Testimoni_model');
		$m_testimoni = new Testimoni_model();
		$kode_testimoni = htmlspecialchars($this->input->post('kode_testimoni'));
		$valid = $this->db->query("SELECT * FROM tbl_malond_testimoni WHERE kode_testimoni = '$kode_testimoni'")->num_rows();
		$get_file_name	= $this->db->query("SELECT * FROM tbl_malond_testimoni WHERE kode_testimoni = '$kode_testimoni'")->row();
		if ($valid > 0) {
			$dir_remove_file_old = "./assets_default/img_kerjasama/".$get_file_name->nama_file;
			if (file_exists($dir_remove_file_old)) {
				unlink($dir_remove_file_old);
			}
			$m_testimoni->delete_data_testimoni('tbl_malond_testimoni', $kode_testimoni);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Testimoni berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Testimoni gagal di hapus.');
			redirect($this->agent->referrer());
		}
	}


	// public function action_input_testimoni_video() {
	// 	$this->load->model('Testimoni_model');
	// 	$m_testimoni = Testimoni_model();

	// 	$nama_testimoni   = htmlspecialchars($this->input->post('nama_testimoni'));
	// 	$kode_link        = htmlspecialchars($this->input->post('kode_link'));
	// 	$lokasi_testimoni = htmlspecialchars($this->input->post('lokasi_testimoni'));

	// 	$valid = $this->db->query("SELECT * FROM tbl_malond_testimoni_video WHERE kode_link_testimoni = '$kode_link'")->num_rows();

	// 	if ($valid > 0) {

	// 		$data_testimoni_video = array(
	// 			'nama_testimoni' => $nama_testimoni,
	// 			'kode_link'	=> $kode_link,
	// 			'lokasi_testimoni' => $lokasi_testimoni,
	// 			'tanggal_input' => date("Y-m-d H:i:s")
	// 		);

	// 		$m_testimoni->view_all_testimoni_video('tbl_malond_testimoni_video', $data_testimoni_video);
	// 		$this->session->set_flashdata('message_data', '<strong>Success</strong> Testimoni video berhasil di tambahkan.');
	// 		redirect($this->agent->referrer());

	// 	} else {
	// 		$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Video testimoni ini tersedia.');
	// 		redirect($this->agent->referrer());
	// 	}
	// }

	// public function action_update_testimoni_video() {
	// 	$this->load->model('Testimoni_model');
	// 	$m_testimoni = new Testimoni_model();

	// 	$kode_testimoni_video = htmlspecialchars($this->input->post('kode_testimoni_video'));
	// 	$nama_testimoni = htmlspecialchars($this->input->post('nama_testimoni'));
	// 	$kode_link      = htmlspecialchars($this->input->post('kode_link'));
	// 	$lokasi_testimoni = htmlspecialchars($this->input->post('lokasi_testimoni'));

	// 	$valid = $this->db->query("SELECT * FROM tbl_malond_testimoni_video WHERE kode_testimoni_video = '$kode_testimoni_video'")->num_rows();
	// 	if ($valid > 0) {
	// 		$data_testimoni = array(
	// 			'nama_testimoni' => $nama_testimoni,
	// 			'kode_link' => $kode_link,
	// 			'lokasi_testimoni' => $lokasi_testimoni
	// 		);

	// 		$m_testimoni->insert_data_testimoni_video('tbl_malond_testimoni_video', $kode_testimoni_video, $data_testimoni);
	// 		$this->session->set_flashdata('message_data', '<strong>Success</strong> Testimoni video berhasil di ubah.');
	// 		redirect($this->agent->referrer());

	// 	} else {
	// 		$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Gagal update testimoni.');
	// 		redirect($this->agent->referrer());
	// 	}

	// }

	// public function action_delete_testimoni_video($kode_testimoni_video) {
	// 	$this->load->model('Testimoni_model');
	// 	$m_testimoni = new Testimoni_model();

	// 	$valid = $this->db->query("SELECT * FROM tbl_malond_testimoni_video WHERE kode_testimoni_video = '$kode_testimoni_video'")->num_rows();
	// 	if ($valid > 0) {
	// 		$m_testimoni->delete_data_testimoni_video('tbl_malond_testimoni_video', $kode_testimoni_video);
	// 		$this->session->set_flashdata('message_data', '<strong>Success</strong> Testimoni video berhasil di hapus.');
	// 		redirect($this->agent->referrer());
	// 	} else {
	// 		$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Gagal hapus testimoni.');
	// 		redirect($this->agent->referrer());
	// 	}
	// }

	public function action_insert_kerjasama() {
		$this->load->model('Kerjasama_model');
		$m_kerjasama = new Kerjasama_model();

		$nama_partner = htmlspecialchars($this->input->post('nama_partner'));
		$link_partner = htmlspecialchars($this->input->post('link_partner'));
		$file_name    = $_FILES['logo_partner']['name'];
		$error = $_FILES['logo_partner']['error'];
		$remove_strip_nama_gambar = str_replace("-"," ", $nama_partner);
		$nama_gambar_slug   = $this->slug($remove_strip_nama_gambar);
		$nama_gambar   		= str_replace("-","_", $nama_gambar_slug);

		if ($error > 0) {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Kerjasama gagal di tambahkan file tidak ada.');
			redirect($this->agent->referrer());
		} else {
			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = date('Y_d_s').'_'.$nama_gambar.'.'.$file_type; 

			$config['upload_path'] = './assets_default/img_kerjasama/';
			$config['allowed_types'] = '*';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('logo_partner')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Kerjasama gagal di tambahkan.');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}
		}

		$data_kerjasama = array(
							'nama_partner' => $nama_partner,
							'link_partner' => $link_partner,
							'logo_name_partner' => $new_file_name,
							'create_at' => date("Y-m-d H:i:s")
						);
		$m_kerjasama->insert_data_kerjasama('tbl_malond_kerjasama',$data_kerjasama);
		$this->session->set_flashdata('message_data', '<strong>Success</strong> Kerjasama berhasil di tambahkan.');
		redirect($this->agent->referrer());
	}

	public function action_update_kerjasama() {
		$this->load->model('Kerjasama_model');
		$m_kerjasama = new Kerjasama_model();

		$nama_partner 	= htmlspecialchars($this->input->post('nama_partner'));
		$link_partner 	= htmlspecialchars($this->input->post('link_partner'));
		$img_old 		= htmlspecialchars($this->input->post('img_old'));
		$kode_kerjasama = htmlspecialchars($this->input->post('kode_kerjasama'));

		$valid = $this->db->query("SELECT * FROM tbl_malond_kerjasama WHERE kode_kerjasama = '$kode_kerjasama'")->num_rows();
		if ($valid > 0) {

			if ($_FILES['logo_partner']['tmp_name'] == '') {
				$data_kerjasama = array(
							'nama_partner' => $nama_partner,
							'link_partner' => $link_partner,
							'logo_name_partner' => $img_old,
							'create_at' => date("Y-m-d H:i:s")
						);
				$m_kerjasama->update_data_kerjasama('tbl_malond_kerjasama', $kode_kerjasama ,$data_kerjasama);
				$this->session->set_flashdata('message_data', '<strong>Success</strong> Kerjasama berhasil di ubah.');
				redirect($this->agent->referrer());
			} else {
				$dir_remove_file_old = "./assets_default/img_kerjasama/".$img_old;
				if (file_exists($dir_remove_file_old)) {
					unlink($dir_remove_file_old);
				}

				$file_name    				= $_FILES['logo_partner']['name'];
				$error 						= $_FILES['logo_partner']['error'];
				$remove_strip_nama_gambar 	= str_replace("-"," ", $nama_partner);
				$nama_gambar_slug   			= $this->slug($remove_strip_nama_gambar);
				$nama_gambar   				= str_replace("-","_", $nama_gambar_slug);

				$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
				$new_file_name = date('Y_d_s').'_'.$nama_gambar.'.'.$file_type; 

				$config['upload_path'] = './assets_default/img_kerjasama/';
				$config['allowed_types'] = '*';
				$config['max_size']  = '*';
				$config['file_name'] = $new_file_name;
				
				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload('logo_partner')){
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Kerjasama gagal di ubah.');
					redirect($this->agent->referrer());
				}
				else{
					$data = array('upload_data' => $this->upload->data());
				}
			}

			$data_kerjasama = array(
								'nama_partner' => $nama_partner,
								'link_partner' => $link_partner,
								'logo_name_partner' => $new_file_name,
								'create_at' => date("Y-m-d H:i:s")
							);
			$m_kerjasama->update_data_kerjasama('tbl_malond_kerjasama', $kode_kerjasama ,$data_kerjasama);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Kerjasama berhasil di ubah.');
			redirect($this->agent->referrer());

		} else {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Data kerja sama tak tersedia.');
			redirect($this->agent->referrer());
		}
	}

	public function action_delete_kerjasama($kode_kerjasama) {
		$this->load->model('Kerjasama_model');
		$m_kerjasama = new Kerjasama_model();

		$valid = $this->db->query("SELECT * FROM tbl_malond_kerjasama WHERE kode_kerjasama = '$kode_kerjasama'")->num_rows();
		$get_file_name	= $this->db->query("SELECT * FROM tbl_malond_kerjasama WHERE kode_kerjasama = '$kode_kerjasama'")->row();
		if ($valid > 0) {

			$dir_remove_file_old = "./assets_default/img_kerjasama/".$get_file_name->logo_name_partner;
			if (file_exists($dir_remove_file_old)) {
				unlink($dir_remove_file_old);
			}
			$m_kerjasama->delete_data_kerjasama('tbl_malond_kerjasama', $kode_kerjasama);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Data kerjasama berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Data kerja sama tak tersedia.');
			redirect($this->agent->referrer());
		}
	}

	public function action_input_sales() {
		$this->load->model('Sales_model');
		$m_sales = new Sales_model();

		$nama_sales 	= htmlspecialchars($this->input->post('nama_sales'));
		$posisi_sales   = htmlspecialchars($this->input->post('posisi_sales'));
		$nomor_telepon_sales = htmlspecialchars($this->input->post('nomor_telepon_sales'));

		$file_name    		= $_FILES['logo_sales']['name'];
		$error 				= $_FILES['logo_sales']['error'];
		$remove_strip_nama_gambar = str_replace("-"," ", $nama_sales);
		$nama_gambar_slug   = $this->slug($remove_strip_nama_gambar);
		$nama_gambar   		= str_replace("-","_", $nama_gambar_slug);


		if ($error > 0) {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Sales gagal di tambahkan foto tidak ada.');
			redirect($this->agent->referrer());
		} else {

			$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
			$new_file_name = date('Y_d_s').'_'.$nama_gambar.'.'.$file_type; 

			$config['upload_path'] = './assets_default/img_sales/';
			$config['allowed_types'] = '*';
			$config['max_size']  = '*';
			$config['file_name'] = $new_file_name;
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('logo_sales')){
				$error = array('error' => $this->upload->display_errors());
				$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Sales gagal di tambahkan.');
				redirect($this->agent->referrer());
			}
			else{
				$data = array('upload_data' => $this->upload->data());
			}

			$data_sales = array(
							'nama_sales' 		=> $nama_sales,
							'posisi_sales' 		=> $posisi_sales,
							'nomor_telepon_sales' => $nomor_telepon_sales,
							'foto_sales' 		=> $new_file_name
						);
			$m_sales->insert_data_sales('tbl_malond_sales', $data_sales);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Sales berhasil di tambahkan.');
			redirect($this->agent->referrer());
		}
	}

	public function action_update_sales() {
	}

	public function action_delete_sales() {
	}

	public function action_delete_inbox($kode_inbox) {
		$this->load->model('Inbox_model');
		$m_inbox = new Inbox_model();

		$valid = $this->db->query("SELECT * FROM tbl_malond_pesan WHERE kode_inbox = '$kode_inbox'")->num_rows();
		if ($valid > 0) {
			$m_inbox->delete_data_inbox('tbl_malond_pesan', $kode_inbox);
			$this->session->set_flashdata('message_data', '<strong>Success</strong> Pesan berhasil di hapus.');
			redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('error_data', '<strong>Ups!!! </strong> Pesan gagal di hapus');
			redirect($this->agent->referrer());
		}
	}

	public function slug($string, $space="-") {
        $string = preg_replace("/[^a-zA-Z0-9 \-]/", "", $string);
        $string = trim(preg_replace("/\\s+/", " ", $string));
        $string = strtolower($string);
        $string = str_replace(" ", $space, $string);

        return $string;
    }

}

/* End of file Administrator.php */
/* Location: ./application/controllers/Administrator.php */