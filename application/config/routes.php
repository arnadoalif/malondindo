<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'frontpage';
$route['404_override'] = 'frontpage';
$route['translate_uri_dashes'] = FALSE;

$route['profil'] 		= "frontpage/profile";
$route['produk'] 		= "frontpage/produk";
$route['testimoni'] 	= "frontpage/testimoni";
$route['resep'] 		= "frontpage/resep";
$route['resep/detail-resep/(:any)'] = "frontpage/detail_resep/$1";
$route['kontak']		= "frontpage/kontak";

$route['admin']                      = 'administrator';
$route['admin/pelanggan-order'] 	 = 'administrator/view_order';
$route['admin/produk']          	 = 'administrator/view_produk';
$route['admin/galeri']				 = 'administrator/view_galeri';

$route['admin/testimoni']            = 'administrator/view_testimoni';
$route['admin/testimoni_personal']   = 'administrator/view_testimoni_personal';
$route['admin/testimoni_kostumer']   = 'administrator/view_testimoni_kostumer';
$route['admin/testimoni_video']		 = 'administrator/view_testimoni_video';

$route['admin/kerjasama']			 = 'administrator/view_kerjasama';
$route['admin/inbox']			     = 'administrator/view_inbox';
$route['admin/sales']			     = 'administrator/view_setting_sales';
$route['admin/resep']				 = 'administrator/view_resep';

$route['admin/produk/input']		 = 'administrator/input_data_produk';
$route['admin/galeri/input']		 = 'administrator/input_data_galeri';
$route['admin/galeri/categori/input'] = 'administrator/input_data_categori';
$route['admin/testimoni/input']	     = 'administrator/input_data_testimoni';
$route['admin/kerjasama/input']	     = 'administrator/input_data_kerjasama';
$route['admin/resep/input']			 = 'administrator/input_data_resep';

$route['admin/resep/edit/(:any)']    = 'administrator/view_edit_resep/$1'; 

$route['login/2017']  = 'login';
$route['login']  = 'login';
$route['out'] = 'login/logout';

$route['translate_uri_dashes'] = FALSE;