<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Login Administrator</title>

    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>

    <div class="am-signin-wrapper">
      <div class="am-signin-box">
        <div class="row no-gutters">
          <div class="col-lg-5">
            <div>
              <img src="<?php echo base_url('assets_default/logo-malon-white-bg.png'); ?>" class="img-responsive" alt="Image" style="max-width: 80%;">
              
              <hr>
              <p>PT. MALOND INDO PERKASA</p>
            </div>
          </div>
          <div class="col-lg-7">
            <h5 class="tx-gray-800 mg-b-25">Signin to Your Account</h5>
            
            <?php if (isset($_SESSION['message_data'])): ?>
              <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                <?php echo $_SESSION['message_data'] ?>
              </div>
            <?php endif ?>

            <?php if (isset($_SESSION['error_data'])): ?>
              <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <?php echo $_SESSION['error_data'] ?>
              </div>
            <?php endif ?>

            <form action="<?php echo base_url('login/action_login'); ?>" method="POST" accept-charset="utf-8">
          
              <div class="form-group">
                <label class="form-control-label">Username :</label>
                <input type="text" name="user_name" class="form-control" placeholder="Please enter your username">
              </div><!-- form-group -->

              <div class="form-group">
                <label class="form-control-label">Password:</label>
                <input type="password" name="password" class="form-control" placeholder="Please enter your password">
              </div><!-- form-group -->

              <button type="submit" class="btn btn-block">Sign In</button>
            </form>
          </div><!-- col-7 -->
        </div><!-- row -->
        <p class="tx-center tx-white-5 tx-12 mg-t-15">Copyright 2018 &copy;. All Rights Reserved. PT MALOND INDO PERKASA. Created by: Devisi Promo</p>
      </div><!-- signin-box -->
    </div><!-- am-signin-wrapper -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>
  </body>
</html>
