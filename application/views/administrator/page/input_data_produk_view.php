<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>

    <?php require_once(APPPATH .'views/administrator/header.php'); ?>

    <?php require_once(APPPATH .'views/administrator/menu_side.php'); ?>

    <div class="am-mainpanel">
      <div class="am-pagetitle">
        <h5 class="am-title">INPUT DATA PRODUK</h5>
        
      </div><!-- am-pagetitle -->

      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">INPUT PRODUK MALON</h6>
          <p class="mg-b-20 mg-sm-b-30"></p>

          <div class="form-layout">
            <div class="row mg-b-25">
              <div class="col-lg-8">
                <div class="form-group">
                  <label class="form-control-label">Nama Produk: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" required name="nama_produk" required value="" placeholder="Nama Produk">
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label">Berat Produk <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" required name="berat_produk" value="" placeholder="Barat Produk">
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-2">
                <div class="form-group">
                  <label class="form-control-label">Isi / Pack: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" required name="isi_produk" value="" placeholder="Isi Produk">
                </div>
              </div><!-- col-4 -->

              <div class="col-lg-6 mg-t-40 mg-lg-t-0">
                <label class="form-control-label">Cover Produk <span class="tx-danger">*</span></label>
                <br>
                <label class="custom-file">
                  <input type="file" id="file2" class="custom-file-input">
                  <span class="custom-file-control custom-file-control-primary"></span>
                </label>
              </div><!-- col -->

              <div class="col-lg-6 mg-t-40 mg-lg-t-0">
                <label class="form-control-label">Cover Album Produk: <span class="tx-danger">*</span></label>
                <br>
                <label class="custom-file">
                  <input type="file" id="file2" class="custom-file-input">
                  <span class="custom-file-control custom-file-control-primary"></span>
                </label>
              </div><!-- col -->

              <div class="col-lg-12">
                <div class="form-group">
                  <label class="form-control-label">Keterangan Produk <span class="tx-danger">*</span></label>
                  <textarea name="keterangan_produk" id="inputKeterangan_produk" class="form-control" rows="10" required="required"></textarea>
                </div>
              </div><!-- col-4 -->

            </div><!-- row -->

            <div class="form-layout-footer">
              <button try class="btn btn-info mg-r-5">Submit Form</button>
              <button class="btn btn-secondary">Cancel</button>
            </div><!-- form-layout-footer -->
          </div><!-- form-layout -->
        </div><!-- card -->

      </div><!-- am-pagebody -->
      <?php require_once(APPPATH .'views/administrator/footer.php'); ?>
    </div><!-- am-mainpanel -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>
  </body>
</html>
