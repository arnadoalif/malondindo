<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>

    <?php require_once(APPPATH .'views/administrator/header.php'); ?>

    <?php require_once(APPPATH .'views/administrator/menu_side.php'); ?>

    <div class="am-mainpanel">
      <div class="am-pagetitle">
        <h5 class="am-title">DATA KERJASAMA</h5>
      </div><!-- am-pagetitle -->

      <div class="am-pagebody">
        <a class="btn btn-primary" href="<?php echo base_url('admin/kerjasama/input'); ?>" role="button"> <i class="fa fa-plus"></i> TAMBAH KERJASAMA </a>
        <br><br>

        <?php if (isset($_SESSION['message_data'])): ?>
          <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $_SESSION['message_data'] ?>
          </div>
        <?php endif ?>

        <?php if (isset($_SESSION['error_data'])): ?>
          <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $_SESSION['error_data'] ?>
          </div>
        <?php endif ?>


        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">Data Kerjasama</h6>
          <p class="mg-b-20 mg-sm-b-30"></p>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th>#</th>
                  <th class="wd-15p">Logo</th>
                  <th class="wd-15p">Nama Kerjasama</th>
                  <th class="wd-15p">Link Web</th>
                  <th class="wd-20p">Tanggal Upload</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; foreach ($data_kerjasama as $dt_kerjasama): ?>
                <tr>
                  <td><?php echo $i++; ?></td>
                  <td><img style="max-width: 45%;" src="<?php echo base_url("assets_default/img_kerjasama/$dt_kerjasama->logo_name_partner"); ?>" class="img-responsive img-thumbnail" alt="Image"> </td>
                  <td><?php echo $dt_kerjasama->nama_partner ?></td>
                  <td><?php echo $dt_kerjasama->link_partner ?></td>
                  <td><?php echo date("d/m/Y", strtotime( $dt_kerjasama->create_at)); ?></td>
                  <td>
                    <a class="btn btn-primary" data-toggle="modal" href="#modal-id<?php echo $dt_kerjasama->kode_kerjasama ?>" role="button">Edit</a>
                    <a class="btn btn-danger" href="<?php echo base_url('administrator/action_delete_kerjasama/'.$dt_kerjasama->kode_kerjasama);  ?>" role="button">Hapus</a>
                  </td>
                </tr>

                <div class="modal fade" id="modal-id<?php echo $dt_kerjasama->kode_kerjasama ?>">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content tx-size-sm">
                      <div class="modal-header pd-x-20">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Edit Data Kerjasama <?php echo $dt_kerjasama->nama_partner ?></h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body pd-20">
                        <form action="<?php echo base_url('administrator/action_update_kerjasama'); ?> " method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                          
                          <div class="row">
                            <label class="col-sm-3 form-control-label">Nama Partner : <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                              <input type="text" class="form-control" required name="nama_partner" value="<?php echo $dt_kerjasama->nama_partner; ?>" placeholder="Nama">
                              <input type="hidden" class="form-control" required name="kode_kerjasama" value="<?php echo $dt_kerjasama->kode_kerjasama; ?>">
                            </div>
                          </div><!-- row -->
                          <div class="row mg-t-20">
                            <label class="col-sm-3 form-control-label">Link Partner: <span class="tx-danger">*</span></label>
                              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="url" class="form-control" name="link_partner" value="<?php echo $dt_kerjasama->link_partner; ?>" placeholder="Link Partner">
                              </div>
                          </div>
                          <div class="row mg-t-20">
                            <label class="col-sm-3 form-control-label">Logo Partner: <span class="tx-danger">*</span></label>
                              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <label class="custom-file">
                                  <input type="file" id="file2" name="logo_partner" accept=".png, .jpg, .jpeg" class="custom-file-input">
                                  <input type="hidden" name="img_old" id="input" class="form-control" value="<?php echo $dt_kerjasama->logo_name_partner ?> ">
                                  <span class="custom-file-control custom-file-control-primary"></span>
                                </label>
                              </div>
                          </div>
                          
                          
                        </div><!-- modal-body -->
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-info pd-x-20">Save changes</button>
                          <button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Close</button>
                        </div>
                      </form>

                    </div>
                  </div><!-- modal-dialog -->
                </div><!-- modal -->

                
                  

                <?php endforeach ?>
               
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->

      </div><!-- am-pagebody -->
      <?php require_once(APPPATH .'views/administrator/footer.php'); ?>
    </div><!-- am-mainpanel -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>
  </body>
</html>
