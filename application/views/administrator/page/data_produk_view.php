<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>

    <?php require_once(APPPATH .'views/administrator/header.php'); ?>

    <?php require_once(APPPATH .'views/administrator/menu_side.php'); ?>

    <div class="am-mainpanel">
      <div class="am-pagetitle">
        <h5 class="am-title">DATA PRODUK</h5>
      </div><!-- am-pagetitle -->

      <div class="am-pagebody">
        <a class="btn btn-primary" href="<?php echo base_url('admin/produk/input'); ?>" role="button"> <i class="fa fa-plus"></i> TAMBAH PRODUK </a>
        <br><br>
        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">Data Produk Periode <?php echo $periode ?></h6>
          <p class="mg-b-20 mg-sm-b-30"></p>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Nama Produk</th>
                  <th class="wd-15p">Berat</th>
                  <th class="wd-20p">Isi/Pack</th>
                  <th class="wd-20p">Stok Ekor</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($data_stok_malond as $dt_malon): ?>
                  
                <tr>
                  <td><?php echo $dt_malon->NamaBrg ?></td>
                  <td>Berat <?php echo "<strong>".$dt_malon->BeratMin.'</strong> - <strong>'.$dt_malon->BeratMax ."</strong> gram" ?></td>
                  <td><?php echo number_format($dt_malon->Pack,0,",",".")." ekor";?></td>
                  <td><?php echo number_format($dt_malon->Saldo_Ekor,0,",","."); ?></td>
                </tr>

                <?php endforeach ?>
               
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->

      </div><!-- am-pagebody -->
      <?php require_once(APPPATH .'views/administrator/footer.php'); ?>
    </div><!-- am-mainpanel -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>
  </body>
</html>
