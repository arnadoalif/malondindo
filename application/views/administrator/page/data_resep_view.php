<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>

    <?php require_once(APPPATH .'views/administrator/header.php'); ?>

    <?php require_once(APPPATH .'views/administrator/menu_side.php'); ?>

    <div class="am-mainpanel">
      <div class="am-pagetitle">
        <h5 class="am-title">DATA RESEP</h5>
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <a class="btn btn-primary" href="<?php echo base_url('admin/resep/input'); ?>" role="button"> <i class="fa fa-plus"></i> TAMBAH RESEP </a>
        <br><br>
        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">DATA RESEP</h6>
          <p class="mg-b-20 mg-sm-b-30"></p>
          
          <?php if (isset($_SESSION['message_data'])): ?>
            <div class="alert alert-success" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <?php echo $_SESSION['message_data'] ?>
            </div>
          <?php endif ?>

          <?php if (isset($_SESSION['error_data'])): ?>
            <div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <?php echo $_SESSION['error_data'] ?>
            </div>
          <?php endif ?>

          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Nama Resep</th>
                  <th class="wd-15p">Link Resep</th>
                  <th class="wd-20p">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($data_resep as $dt_resep): ?>
                  <tr>
                    <td><?php echo $dt_resep->nama_resep; ?></td>
                    <td><a target="_blank" href="<?php echo "https://youtu.be/".$dt_resep->link_id_resep_youtube ?>" title="">https://youtu.be/<?php echo $dt_resep->link_id_resep_youtube ?></a></td>
                    <td>
                      <a class="btn btn-info" href="<?php echo base_url('admin/resep/edit/'.$dt_resep->kode_resep); ?>" role="button">Update Resep</a>
                      <a class="btn btn-danger" href="<?php echo base_url('administrator/action_delete_resep/'.$dt_resep->link_id_resep_youtube); ?>" role="button">Delete Resep</a>
                    </td>
                  </tr>

                <?php endforeach ?>
               
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->

      </div><!-- am-pagebody -->
      <?php require_once(APPPATH .'views/administrator/footer.php'); ?>
    </div><!-- am-mainpanel -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>
    <script>
      $(function(){
        'use strict';

        // Inline editor
        var editor = new MediumEditor('.editable');

        // Summernote editor
        $('#editor').summernote({
          height: 550,
          tooltip: false
        })
      });
    </script>
  </body>
</html>
