<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>

    <?php require_once(APPPATH .'views/administrator/header.php'); ?>

    <?php require_once(APPPATH .'views/administrator/menu_side.php'); ?>

    <div class="am-mainpanel">
      <div class="am-pagetitle">
        <h5 class="am-title">TAMBAH CATEGORI GALERI</h5>
        
      </div><!-- am-pagetitle -->

      <div class="am-pagebody">
        <div class="row row-sm mg-t-20">
          <div class="col-xl-12">
            <div class="card pd-20 pd-sm-40 form-layout form-layout-4">
              <h6 class="card-body-title">TAMBAH CATEGORI</h6>
              <p class="mg-b-20 mg-sm-b-30">Kategori untuk mengelompokan gallery</p>

              <?php if (isset($_SESSION['message_data'])): ?>
                <div class="alert alert-success" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                   <?php echo $_SESSION['message_data'] ?>
                  </div>
              <?php endif ?>

              <?php if (isset($_SESSION['error_data'])): ?>
                <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                    <?php echo $_SESSION['error_data'] ?>
                </div>
              <?php endif ?>

              <form action="<?php echo base_url('administrator/action_input_categori_galeri');?> " method="POST" accept-charset="utf-8">
                <div class="row">
                  <label class="col-sm-2 form-control-label">Nama Kategori <span class="tx-danger">*</span></label>
                  <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                    <input type="text" class="form-control" required name="nama_kategori" placeholder="Nama Kategori">
                  </div>
                </div><!-- row -->

                <div class="form-layout-footer mg-t-30">
                  <button type="submit" class="btn btn-info mg-r-5"><i class="fa fa-save"></i> Simpan Kategori Galeri</button>
                  <button type="reset" class="btn btn-secondary">Reset</button>
                  <a class="btn btn-warning" href="<?php echo base_url('admin/galeri'); ?>" role="button"><i class="fa fa-home"></i> Kembali </a>
                </div><!-- form-layout-footer -->
            </form>


            <div class="card pd-20 pd-sm-40">
              <h6 class="card-body-title">Data Kategori Galeri</h6>
              <p class="mg-b-20 mg-sm-b-30"></p>
              <div class="table-wrapper">
                <table id="datatable1" class="table display responsive nowrap">
                  <thead>
                    <tr>
                      <th class="wd-15p">Nama Kategori</th>
                      <th class="wd-15p">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($data_categori as $dt_kategori): ?>
                      <tr>
                        <td><?php echo $dt_kategori->nama_kategori ?></td>
                        <td>
                          <a class="btn btn-primary btn-sm" data-toggle="modal" href='#modal-id<?php echo $dt_kategori->kode_kategori_galeri ?>'>Update</a>
                          <a class="btn btn-danger btn-sm" href="<?php echo base_url('administrator/action_delete_categori_galeri/'.$dt_kategori->kode_kategori_galeri); ?>">Delete</a>
                        </td>
                      </tr>

                    <form action="<?php echo base_url('administrator/action_update_categori_galeri') ?> " method="POST" accept-charset="utf-8">
                      <div class="modal fade" id="modal-id<?php echo $dt_kategori->kode_kategori_galeri ?>">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">Edit Kategori <?php echo $dt_kategori->nama_kategori; ?></h4>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <label class="col-sm-4 form-control-label">Nama Kategori <span class="tx-danger">*</span></label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                  <input type="hidden" class="form-control" required name="kode_kategori_galeri" value="<?php echo $dt_kategori->kode_kategori_galeri; ?>" placeholder="Nama Kategori">
                                  <input type="text" class="form-control" required name="nama_kategori" value="<?php echo $dt_kategori->nama_kategori; ?>" placeholder="Nama Kategori">
                                </div>
                              </div><!-- row -->
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>


                    <?php endforeach ?>
                  </tbody>
                </table>
                </div><!-- table-wrapper -->
                </div><!-- card -->

            </div><!-- card -->
          </div><!-- col-12 -->
        </div><!-- row -->

      </div><!-- am-pagebody -->
      <?php require_once(APPPATH .'views/administrator/footer.php'); ?>
    </div><!-- am-mainpanel -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>
  </body>
</html>
