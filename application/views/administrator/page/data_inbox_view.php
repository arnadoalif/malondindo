<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>

    <?php require_once(APPPATH .'views/administrator/header.php'); ?>

    <?php require_once(APPPATH .'views/administrator/menu_side.php'); ?>

    <div class="am-mainpanel">
      <div class="am-pagetitle">
        <h5 class="am-title">DATA INBOX</h5>
      </div><!-- am-pagetitle -->

      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">Data Inbox</h6>
          <p class="mg-b-20 mg-sm-b-30"></p>
          
          <?php if (isset($_SESSION['message_data'])): ?>
              <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <?php echo $_SESSION['message_data'] ?>
              </div>
          <?php endif ?>

          <?php if (isset($_SESSION['error_data'])): ?>
              <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <?php echo $_SESSION['error_data'] ?>
              </div>
          <?php endif ?>

          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Nama Inbox</th>
                  <th class="wd-15p">Nomor Telepon</th>
                  <th class="wd-15p">E-Mail</th>
                  <th class="wd-20p">Tanggal Inbox</th>
                  <th class="wd-15p">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($data_inbox as $dt_inbox): ?>
                  
                <tr>
                  <td><?php echo $dt_inbox->nama_inbox ?></td>
                  <td><?php echo $dt_inbox->nomor_telepon ?></td>
                  <td><?php echo $dt_inbox->email_inbox ?></td>
                  <td><?php echo date("d/m/Y", strtotime( $dt_inbox->create_at)); ?></td>
                  <td>
                    <a class="btn btn-sm btn-info" href="#modal-id<?php echo $dt_inbox->kode_inbox ?>" data-toggle="modal"><i class="fa fa-eye"></i></a>
                    <a class="btn btn-sm btn-danger" href="<?php echo base_url('administrator/action_delete_inbox/'.$dt_inbox->kode_inbox); ?>" role="button"> <i class="fa fa-trash"></i> </a>
                  </td>
                </tr>

                <div class="modal fade" id="modal-id<?php echo $dt_inbox->kode_inbox ?>">
                  <div class="modal-dialog ">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><?php echo $dt_inbox->nama_inbox ?></h4>
                      </div>
                      <div class="modal-body">
                        <p style="color: #000;"><?php echo $dt_inbox->isi_inbox ?></p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                      </div>
                    </div>
                  </div>
                </div>

                <?php endforeach ?>
               
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->

      </div><!-- am-pagebody -->
      <?php require_once(APPPATH .'views/administrator/footer.php'); ?>
    </div><!-- am-mainpanel -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>
  </body>
</html>
