<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>

    <?php require_once(APPPATH .'views/administrator/header.php'); ?>

    <?php require_once(APPPATH .'views/administrator/menu_side.php'); ?>

    <div class="am-mainpanel">
      <div class="am-pagetitle">
        <h5 class="am-title">INPUT DATA RESEP</h5>
        
      </div><!-- am-pagetitle -->

      <div class="am-pagebody">
        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">INPUT RESEP MALON</h6>
          <p class="mg-b-20 mg-sm-b-30"></p>
          <?php if (isset($_SESSION['message_data'])): ?>
            <div class="alert alert-success" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <?php echo $_SESSION['message_data'] ?>
            </div>
          <?php endif ?>

          <?php if (isset($_SESSION['error_data'])): ?>
            <div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <?php echo $_SESSION['error_data'] ?>
            </div>
          <?php endif ?>
          
          <form action="<?php echo base_url('administrator/action_input_resep'); ?>" method="post">
            <div class="form-layout">
              <div class="row mg-b-25">
               
                  
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label">Nama Resep: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" required name="nama_resep" required value="" placeholder="Nama Resep">
                  </div>
                </div><!-- col-4 -->

                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label">Kode Share Youtube <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" required name="kode_link" required value="" placeholder="Kode Link Youtube">
                    <p class="mg-b-10 mg-t-10"><s>https://youtu.be/</s><span style="color: red; font-style: italic; font-weight: 600;">Xj8Pqlj9Vbc</span> copy dan paste id ke form</p>
                  </div>
                </div><!-- col-4 -->

                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label">Keterangan Resep <span class="tx-danger">*</span></label>
                    <textarea name="keterangan_resep" id="summernote" class="form-control" rows="50" required="required"></textarea>
                  </div>
                </div><!-- col-4 -->

              </div><!-- row -->

              <div class="form-layout-footer">
                <button type="submit" class="btn btn-info mg-r-5">Submit Form</button>
                <button class="btn btn-secondary">Cancel</button>
              </div><!-- form-layout-footer -->

            </div><!-- form-layout -->
          </form>
        </div><!-- card -->

      </div><!-- am-pagebody -->
      <?php require_once(APPPATH .'views/administrator/footer.php'); ?>
    </div><!-- am-mainpanel -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>
  </body>
</html>
