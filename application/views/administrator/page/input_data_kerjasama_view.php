<!DOCTYPE html>
<html lang="en">
  <head>
    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>
    <?php require_once(APPPATH .'views/administrator/header.php'); ?>
    <?php require_once(APPPATH .'views/administrator/menu_side.php'); ?>

    <div class="am-mainpanel">
      <div class="am-pagetitle">
        <h5 class="am-title">INPUT DATA KERJASAMA</h5>
        
      </div><!-- am-pagetitle -->

      <div class="am-pagebody">
         <div class="row row-sm mg-t-20">
          <div class="col-xl-12">
            <div class="card pd-20 pd-sm-40 form-layout form-layout-4">
              <h6 class="card-body-title">KERJASAMA</h6>
              <p class="mg-b-20 mg-sm-b-30">Partner PT MALOND INDO PERKASA</p>

              <?php if (isset($_SESSION['message_data'])): ?>
                <div class="alert alert-success" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <?php echo $_SESSION['message_data'] ?>
                </div>
              <?php endif ?>

              <?php if (isset($_SESSION['error_data'])): ?>
                <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <?php echo $_SESSION['error_data'] ?>
                </div>
              <?php endif ?>

            <form action="<?php echo base_url('administrator/action_insert_kerjasama'); ?>" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
              <div class="row">
                <label class="col-sm-3 form-control-label">Nama Partner : <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                  <input type="text" class="form-control" required name="nama_partner" placeholder="Nama">
                </div>
              </div><!-- row -->
              <div class="row mg-t-20">
                <label class="col-sm-3 form-control-label">Link Partner: <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                  <input type="url" class="form-control" name="link_partner" placeholder="Link Partner">
                </div>
              </div>
              <div class="row mg-t-20">
                <label class="col-sm-3 form-control-label">Logo Partner: <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                  <label class="custom-file">
                    <input type="file" id="file2" name="logo_partner" accept=".png, .jpg, .jpeg" class="custom-file-input">
                    <span class="custom-file-control custom-file-control-primary"></span>
                  </label>
                </div>
              </div>
              <div class="form-layout-footer mg-t-30">
               <button type="submit" class="btn btn-info mg-r-5"><i class="fa fa-save"></i> Simpan Kerjasama</button>
                  <button type="reset" class="btn btn-secondary">Reset</button>
                  <a class="btn btn-warning" href="<?php echo base_url('admin/kerjasama'); ?>" role="button"><i class="fa fa-home"></i> Kembali </a>
              </div><!-- form-layout-footer -->
            </form>

            </div><!-- card -->
          </div><!-- col-12 -->
        </div><!-- row -->

      </div><!-- am-pagebody -->
      <?php require_once(APPPATH .'views/administrator/footer.php'); ?>
    </div><!-- am-mainpanel -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>
  </body>
</html>
