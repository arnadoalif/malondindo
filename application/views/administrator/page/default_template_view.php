<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>

    <?php require_once(APPPATH .'views/administrator/header.php'); ?>

    <?php require_once(APPPATH .'views/administrator/menu_side.php'); ?>

    <div class="am-mainpanel">
      <div class="am-pagetitle">
        <h5 class="am-title">Dashboard</h5>
        <form id="searchBar" class="search-bar" action="index.html">
          <div class="form-control-wrapper">
            <input type="search" class="form-control bd-0" placeholder="Search...">
          </div><!-- form-control-wrapper -->
          <button id="searchBtn" class="btn btn-orange"><i class="fa fa-search"></i></button>
        </form><!-- search-bar -->
      </div><!-- am-pagetitle -->

      <div class="am-pagebody">
        

      </div><!-- am-pagebody -->
      <?php require_once(APPPATH .'views/administrator/footer.php'); ?>
    </div><!-- am-mainpanel -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>
  </body>
</html>
