<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>

    <?php require_once(APPPATH .'views/administrator/header.php'); ?>

    <?php require_once(APPPATH .'views/administrator/menu_side.php'); ?>

    <div class="am-mainpanel">
      <div class="am-pagetitle">
        <h5 class="am-title">DATA TESTIMONI KOSTUMER</h5>
      </div><!-- am-pagetitle -->

      <div class="am-pagebody">

          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Nama Kostumer</th>
                  <th class="wd-15p">Alamat Kostumer</th>
                  <th class="wd-20p">Nomor Telepon</th>
                  <th class="wd-20p">Pesan</th>
                  <th class="wd-20p">Tanggal Create</th>
                  <th class="wd-20p">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($data_testimoni_kostumer as $dt_testimoni): ?>
                <tr>
                  <td><?php echo $dt_testimoni->nama_kostumer ?></td>
                  <td><?php echo $dt_testimoni->alamat_kostumer ?></td>
      <td><?php echo $dt_testimoni->nomor_telepon ?></td>
      <td><?php echo $dt_testimoni->isi_testimoni ?></td>
                  <td><?php echo date("d/m/Y", strtotime( $dt_testimoni->tanggal_create)); ?></td>
                  <td>
                     <a class="btn btn-info btn-sm" role="button" data-toggle="modal" href='#modal-id<?php echo $dt_testimoni->kode_testimon_kostumer ?>'> <i class="fa fa-eye"></i> </a>
                    <a class="btn btn-info btn-sm" role="button" data-toggle="modal" href='#modal-edit<?php echo $dt_testimoni->kode_testimon_kostumer ?>'> <i class="fa fa-pencil"></i> </a>
                   <!--  <a class="btn btn-danger btn-sm" href="<?php echo base_url('administrator/action_delete_testimoni/'.$dt_testimoni->kode_testimon_kostumer); ?>" role="button"><i class="fa fa-trash"></i></a> -->
                  </td>
                </tr>

                <<!-- div class="modal fade" id="modal-id<?php echo $dt_testimoni->kode_testimon_kostumer ?>">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><?php echo $dt_testimoni->nama_kostumer ?></h4>
                      </div>
                      <div class="modal-body">
                         <p><?php echo $dt_testimoni->pesan_testimoni ?></p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal fade" id="modal-edit<?php echo $dt_testimoni->kode_testimon_kostumer ?>">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><?php echo $dt_testimoni->nama_kostumer ?></h4>
                      </div>
                      <form action="<?php echo base_url('administrator/action_update_testimoni');?> " method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                      <div class="modal-body">
                        <p>Isi Pesan Testimoni</p>
                        <p><?php echo $dt_testimoni->pesan_testimoni; ?></p>
                              <div class="row">
                                <label class="col-sm-2 form-control-label">Nama : <span class="tx-danger">*</span></label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                  <input type="text" class="form-control" required name="nama_kostumer" value="<?php echo $dt_testimoni->nama_kostumer ?>" placeholder="Nama">
                                  <input type="hidden" name="kode_testimoni" value="<?php echo $dt_testimoni->kode_testimoni ?>" placeholder="">
                                </div>
                              </div>
                              <div class="row mg-t-20">
                                <label class="col-sm-2 form-control-label">Alamat: <span class="tx-danger">*</span></label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                   <input type="text" class="form-control" required name="alamat_testimoni" value="<?php echo $dt_testimoni->alamat_testimoni ?>" placeholder="Masukan Alamat">
                                </div>
                              </div>
                              <div class="row mg-t-20">
                                <label class="col-sm-2 form-control-label">Avatar Testimoni : <span class="tx-danger">*</span></label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                  <label class="custom-file">
                                    <input type="file" id="file2" name="logo_testimoni" accept=".png, .jpg, .jpeg" class="custom-file-input">
                                    <input type="hidden" class="form-control" required name="img_old" value="<?php echo $dt_testimoni->nama_file; ?>">
                                    <span class="custom-file-control custom-file-control-primary"></span>
                                  </label>
                                </div>
                              </div>
                              <div class="row mg-t-20">
                                <label class="col-sm-2 form-control-label">Pesan : <span class="tx-danger">*</span></label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                  <textarea rows="7" class="form-control" required name="pesan_testimoni" placeholder="Isi Testimoni">
                                    <?php echo $dt_testimoni->pesan_testimoni ?>
                                  </textarea>
                                </div>
                              </div>
                      </div>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-info mg-r-5">Simpan Testimoni</button>
                        <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                      </div>
                      </form>
                    </div>
                  </div>
                </div> -->

                <?php endforeach ?>
               
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->

      </div><!-- am-pagebody -->
      <?php require_once(APPPATH .'views/administrator/footer.php'); ?>
    </div><!-- am-mainpanel -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>
  </body>
</html>
