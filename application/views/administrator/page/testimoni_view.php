<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>

    <?php require_once(APPPATH .'views/administrator/header.php'); ?>

    <?php require_once(APPPATH .'views/administrator/menu_side.php'); ?>

    <div class="am-mainpanel">
      <div class="am-pagetitle">
        <h5 class="am-title">TESTIMONI</h5>
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
       
        <div class="row">
            <div class="col-sm-6 col-md-4">
              <a class="btn btn-lg btn-primary btn-block mg-b-10"  href="<?php echo base_url('admin/testimoni_personal'); ?>" role="button" style="padding: 30px; font-size: 38px;"><i class="fa fa-child mg-r-10"></i><br> Personal</a>
            </div><!-- col-sm -->
            <div class="col-sm-6 col-md-4 mg-t-20 mg-md-t-0">
              <a class="btn btn-lg btn-success btn-block mg-b-10" href="<?php echo base_url('admin/testimoni_video'); ?>" role="button" style="padding: 30px; font-size: 38px;"><i class="fa fa-street-view mg-r-10"></i><br> Personal + Video</a>
            </div><!-- col-sm -->
            <div class="col-sm-6 col-md-4 mg-t-20 mg-md-t-0">
              <button disabled class="btn btn-lg btn-danger btn-block mg-b-10" href="<?php echo base_url('admin/view_testimoni_video'); ?>" role="button" style="padding: 30px; font-size: 38px;"><i class="fa fa-user mg-r-10"></i><br> Agent </button>
            </div><!-- col-sm -->
        </div>

      </div><!-- am-pagebody -->
      <?php //require_once(APPPATH .'views/administrator/footer.php'); ?>
    </div><!-- am-mainpanel -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>

  </body>
</html>
