<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>

    <?php require_once(APPPATH .'views/administrator/header.php'); ?>

    <?php require_once(APPPATH .'views/administrator/menu_side.php'); ?>

    <div class="am-mainpanel">
      <div class="am-pagetitle">
        <h5 class="am-title">DATA SALES</h5>
      </div><!-- am-pagetitle -->

      <div class="am-pagebody">
        <a class="btn btn-primary" data-toggle="modal" href='#tambah_sales'> <i class="fa fa-plus"></i> TAMBAH SALES </a>
        <br><br>
        
        <?php if (isset($_SESSION['message_data'])): ?>
          <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          <?php echo $_SESSION['message_data'] ?>
          </div>
        <?php endif ?>

        <?php if (isset($_SESSION['error_data'])): ?>
          <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
                </button>
            <?php echo $_SESSION['error_data'] ?>
          </div>
        <?php endif ?>

        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">Data Sales</h6>
          <p class="mg-b-20 mg-sm-b-30"></p>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Nama Sales</th>
                  <th class="wd-15p">Posisi Sales</th>
                  <th class="wd-15p">Nomor Telepon</th>
                  <th class="wd-15p">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($data_sales as $dt_sales): ?>
                  
                <tr>
                  <td><?php echo $dt_sales->nama_sales ?></td>
                  <td><?php echo $dt_sales->posisi_sales ?></td>
                  <td><?php echo $dt_sales->nomor_telepon_sales ?></td>
                  <td>
                    
                  </td>
                </tr>

                <?php endforeach ?>
               
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->

        <div id="tambah_sales" class="modal fade">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
              <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Masukan Data Sales</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form action="<?php echo base_url('administrator/action_input_sales');?> " method="POST" accept-charset="utf-8" enctype="multipart/form-data">
              <div class="modal-body pd-20">
                  <div class="row">
                    <label class="col-sm-3 form-control-label">Nama Sales : <span class="tx-danger">*</span></label>
                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                      <input type="text" class="form-control" required name="nama_sales" placeholder="Nama Sales">
                    </div>
                  </div><!-- row -->
                  <div class="row mg-t-20">
                    <label class="col-sm-3 form-control-label">Posisi: <span class="tx-danger">*</span></label>
                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                       <input type="text" class="form-control" required name="posisi_sales" placeholder="Posisi Sales">
                    </div>
                  </div>
                  <div class="row mg-t-20">
                    <label class="col-sm-3 form-control-label">Nomor Telepon: <span class="tx-danger">*</span></label>
                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                       <input type="text" class="form-control" onkeypress="return isNumberKey(event);" required name="nomor_telepon_sales" placeholder="Nomor Telepon Sales">
                    </div>
                  </div>
                  <div class="row mg-t-20">
                    <label class="col-sm-3 form-control-label">Avatar Sales : <span class="tx-danger">*</span></label>
                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                      <label class="custom-file">
                        <input type="file" id="file2" name="logo_sales" accept=".png, .jpg, .jpeg" class="custom-file-input">
                        <span class="custom-file-control custom-file-control-primary"></span>
                      </label>
                    </div>
                  </div>
              </div><!-- modal-body -->
              <div class="modal-footer">
                <button type="submit" class="btn btn-info pd-x-20">Save Data Sales</button>
                <button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Batal</button>
              </div>
              </form>
            </div>
          </div><!-- modal-dialog -->
        </div><!-- modal -->

      </div><!-- am-pagebody -->
      <?php require_once(APPPATH .'views/administrator/footer.php'); ?>
    </div><!-- am-mainpanel -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>
    <script type="text/javascript">
      
      function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : evt.keyCode
            return !(charCode > 31 && (charCode < 48 || charCode > 57));
        }
    </script>
  </body>
</html>
