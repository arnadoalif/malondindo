<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>

    <?php require_once(APPPATH .'views/administrator/header.php'); ?>

    <?php require_once(APPPATH .'views/administrator/menu_side.php'); ?>

    

    <div class="am-mainpanel">
      <div class="am-pagetitle">
        <h5 class="am-title">Dashboard</h5>
      </div><!-- am-pagetitle -->

      <div class="am-pagebody">

        <!-- <div class="col-lg-12 mg-t-15 mg-sm-t-20 mg-lg-t-0">
            <div class="card pd-20">
              <h6 class="tx-12 tx-uppercase tx-inverse tx-bold mg-b-15">Sale Report</h6>
              <div class="d-flex mg-b-10">
                <div class="bd-r pd-r-10">
                  <label class="tx-12">Januari</label>
                  <p class="tx-lato tx-inverse tx-bold">1,898</p>
                </div>
                <div class="bd-r pd-x-10">
                  <label class="tx-12">Februari</label>
                  <p class="tx-lato tx-inverse tx-bold">32,112</p>
                </div>
                <div class="bd-r pd-x-10">
                  <label class="tx-12">Maret</label>
                  <p class="tx-lato tx-inverse tx-bold">72,067</p>
                </div>
                <div class="bd-r pd-x-10">
                  <label class="tx-12">April</label>
                  <p class="tx-lato tx-inverse tx-bold">72,067</p>
                </div>
                <div class="bd-r pd-x-10">
                  <label class="tx-12">Mei</label>
                  <p class="tx-lato tx-inverse tx-bold">72,067</p>
                </div>
                <div class="bd-r pd-x-10">
                  <label class="tx-12">Juni</label>
                  <p class="tx-lato tx-inverse tx-bold">72,067</p>
                </div>
                <div class="bd-r pd-x-10">
                  <label class="tx-12">Juli</label>
                  <p class="tx-lato tx-inverse tx-bold">72,067</p>
                </div>
                <div class="bd-r pd-x-10">
                  <label class="tx-12">Agustus</label>
                  <p class="tx-lato tx-inverse tx-bold">72,067</p>
                </div>
                <div class="bd-r pd-x-10">
                  <label class="tx-12">September</label>
                  <p class="tx-lato tx-inverse tx-bold">72,067</p>
                </div>
                <div class="bd-r pd-x-10">
                  <label class="tx-12">Oktober</label>
                  <p class="tx-lato tx-inverse tx-bold">72,067</p>
                </div>
                <div class="bd-r pd-x-10">
                  <label class="tx-12">November</label>
                  <p class="tx-lato tx-inverse tx-bold">72,067</p>
                </div>
                <div class="bd-r pd-x-10">
                  <label class="tx-12">Desember</label>
                  <p class="tx-lato tx-inverse tx-bold">72,067</p>
                </div>
              </div>
            </div> 
        </div> -->

        <div class="row row-sm mg-t-15 mg-sm-t-20">

          <div class="col-lg-12">
            <div class="card">
              <div class="card-header bg-transparent pd-20">
                <h6 class="card-title tx-uppercase tx-12 mg-b-0">Data Strok Produk Malon Periode <span class="label label-danger"><?php echo $periode ?></span> </h6>
              </div><!-- card-header -->
              <div class="table-responsive">
                <table class="table table-white mg-b-0 tx-12">
                  <tbody>
                    <?php foreach ($data_stok_malond as $dt_malon): ?>
                      <tr>
                        <td class="pd-l-20 tx-center">
                          <?php if ($dt_malon->NamaBrg == "Malon Perancis Grade A"): ?>
                            <img src="<?php echo base_url('assets_default/malon/malon_grade_a.png'); ?>" class="wd-36 rounded" alt="Image">
                          <?php elseif($dt_malon->NamaBrg == "Malon Perancis Grade B"): ?>
                            <img src="<?php echo base_url('assets_default/malon/malon_grade_b.png'); ?>" class="wd-36 rounded" alt="Image">
                          <?php elseif($dt_malon->NamaBrg == "Malon Perancis Grade C"): ?>
                            <img src="<?php echo base_url('assets_default/malon/malon_grade_c.png'); ?> " class="wd-36 rounded" alt="Image">
                          <?php elseif($dt_malon->NamaBrg == "Malon Perancis Grade Super"): ?>
                            <img src="../img/img1.jpg" class="wd-36 rounded" alt="Image">
                          <?php elseif($dt_malon->NamaBrg == "Malon Perancis Grade Resto"): ?>
                            <img src="<?php echo base_url('assets_default/malon/malon_resto.png'); ?>" class="wd-36 rounded" alt="Image">
                          <?php elseif($dt_malon->NamaBrg == "Malon Perancis Grade D"): ?>
                            <img src="<?php echo base_url('assets_default/malon/malon_grade_b.png'); ?>" class="wd-36 rounded" alt="Image">
                          <?php else: ?>
                            <img src="../img/img1.jpg" class="wd-36 rounded-circle" alt="Image">
                          <?php endif ?>
                          
                        </td>
                        <td>
                          <a href="" class="tx-inverse tx-14 tx-medium d-block"><?php echo $dt_malon->NamaBrg ?></a>
                          <span class="tx-11 d-block">Berat <?php echo "<strong>".$dt_malon->BeratMin.'</strong> - <strong>'.$dt_malon->BeratMax ."</strong> gram" ?><strong></span>
                        </td>
                        <td class="tx-12">
                            Isi / Pack
                           <span class="tx-11 d-block"><strong><?php echo number_format($dt_malon->Pack,0,",","."); ?></strong></span>
                        </td>
                        <td class="tx-12">
                            STOCK PACK
                           <span class="tx-11 d-block"><strong><?php echo number_format($dt_malon->Saldo_Pack,0,",","."); ?></strong></span>
                        </td>
                        <td class="tx-12">
                            STOCK EKOR
                           <span class="tx-11 d-block"><strong><?php echo number_format($dt_malon->Saldo_Ekor,0,",","."); ?></strong></span>
                        </td>
                        <td class="tx-12">
                            STOCK EKOR / 60%
                           <span class="tx-11 d-block"><strong>
                              <?php echo  number_format( $dt_malon->Saldo_Ekor + ($dt_malon->Saldo_Ekor * 0.6),0,",","."); ?></strong></span>
                        </td>
                      </tr>
                    <?php endforeach ?>
                    
                    <!-- <tr>
                      <td class="pd-l-20 tx-center">
                        <img src="../img/img2.jpg" class="wd-36 rounded-circle" alt="Image">
                      </td>
                      <td>
                        <a href="" class="tx-inverse tx-14 tx-medium d-block">Malon Grade A</a>
                        <span class="tx-11 d-block">STOCK : 0000</span>
                      </td>
                      <td class="tx-12">
                          STOCK PACK
                         <span class="tx-11 d-block">0000</span>
                      </td>
                      <td class="tx-12">
                          STOCK EKOR
                         <span class="tx-11 d-block">0000</span>
                      </td>
                    </tr>
                    <tr>
                      <td class="pd-l-20 tx-center">
                        <img src="../img/img3.jpg" class="wd-36 rounded-circle" alt="Image">
                      </td>
                      <td>
                        <a href="" class="tx-inverse tx-14 tx-medium d-block">Malon Grade B</a>
                        <span class="tx-11 d-block">STOCK : 0000</span>
                      </td>
                      <td class="tx-12">
                          STOCK PACK
                          <span class="tx-11 d-block">0000</span>
                      </td>
                      <td class="tx-12">
                          STOCK EKOR
                          <span class="tx-11 d-block">0000</span>
                      </td>
                    </tr>
                    <tr>
                      <td class="pd-l-20 tx-center">
                        <img src="../img/img5.jpg" class="wd-36 rounded-circle" alt="Image">
                      </td>
                      <td>
                        <a href="" class="tx-inverse tx-14 tx-medium d-block">Malon Grade C</a>
                        <span class="tx-11 d-block">STOCK : 0000</span>
                      </td>
                      <td class="tx-12">
                          STOCK PACK
                         <span class="tx-11 d-block">0000</span>
                      </td>
                      <td class="tx-12">
                          STOCK EKOR
                         <span class="tx-11 d-block">0000</span>
                      </td>
                    </tr>
                    <tr>
                      <td class="pd-l-20 tx-center">
                        <img src="../img/img4.jpg" class="wd-36 rounded-circle" alt="Image">
                      </td>
                      <td>
                        <a href="" class="tx-inverse tx-14 tx-medium d-block">Malon Grade D</a>
                        <span class="tx-11 d-block">STOCK : 0000</span>
                      </td>
                      <td class="tx-12">
                          STOCK PACK
                         <span class="tx-11 d-block">0000</span>
                      </td>
                      <td class="tx-12">
                          STOCK EKOR
                         <span class="tx-11 d-block">0000</span>
                      </td>
                    </tr> -->
                  </tbody>
                </table>
              </div><!-- table-responsive -->
              
            </div><!-- card -->
          </div><!-- col-8 -->
          
        </div><!-- row -->

      </div><!-- am-pagebody -->
      <?php require_once(APPPATH .'views/administrator/footer.php'); ?>
    </div><!-- am-mainpanel -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>
  </body>
</html>
