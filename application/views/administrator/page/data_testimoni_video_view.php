<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>

    <?php require_once(APPPATH .'views/administrator/header.php'); ?>

    <?php require_once(APPPATH .'views/administrator/menu_side.php'); ?>

    <div class="am-mainpanel">
      <div class="am-pagetitle">
        <h5 class="am-title">DATA TESTIMONI PERSONAL + VIDEO</h5>
      </div><!-- am-pagetitle -->
      
      <div class="am-pagebody">
        <a class="btn btn-primary" data-toggle="modal" href='#modal-id' href="<?php echo base_url('admin/testimoni_video/input'); ?>" role="button"> <i class="fa fa-plus"></i> TAMBAH TESTIMONI </a>
        <br><br>
        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">DATA TESTIMONI</h6>
          <p class="mg-b-20 mg-sm-b-30"></p>
          
          <?php if (isset($_SESSION['message_data'])): ?>
            <div class="alert alert-success" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <?php echo $_SESSION['message_data'] ?>
            </div>
          <?php endif ?>

          <?php if (isset($_SESSION['error_data'])): ?>
            <div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <?php echo $_SESSION['error_data'] ?>
            </div>
          <?php endif ?>

          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Nama Testimoni</th>
                  <th class="wd-15p">Link Testimoni</th>
                  <th class="wd-20p">Action</th>
                </tr>
              </thead>
              <tbody>
               
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->

        <div class="modal fade" id="modal-id">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Testimoni</h4>
              </div>
              <div class="modal-body">
                
                <form action="<?php echo base_url('administrator/action_input_testimoni_video'); ?>" method="post">
                  <div class="form-layout">
                    <div class="row mg-b-25">
                     
                        
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="form-control-label">Nama Testimoni: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" required name="nama_testimoni" required value="" placeholder="Nama Testimoni">
                        </div>
                      </div><!-- col-4 -->

                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="form-control-label">Kode Share Youtube <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" required name="kode_link" required value="" placeholder="Kode Link Youtube">
                          <p class="mg-b-10 mg-t-10"><s>https://youtu.be/</s><span style="color: red; font-style: italic; font-weight: 600;">Xj8Pqlj9Vbc</span> copy dan paste id ke form</p>
                        </div>
                      </div><!-- col-4 -->

                      <div class="col-lg-12">
                        <div class="form-group">
                          <label class="form-control-label">Lokasi Testimoni <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" required name="lokasi_testimoni" required value="" placeholder="Berikan ( - ) jika lokasi kosong">
                        </div>
                      </div><!-- col-4 -->

                    </div><!-- row -->

                    <div class="form-layout-footer">
                      <button type="submit" disabled class="btn btn-info mg-r-5">Tambah Testimoni</button>
                      <button class="btn btn-secondary">Cancel</button>
                    </div><!-- form-layout-footer -->

                  </div><!-- form-layout -->
                </form>

              </div>
              
            </div>
          </div>
        </div>

      </div><!-- am-pagebody -->
      <?php require_once(APPPATH .'views/administrator/footer.php'); ?>
    </div><!-- am-mainpanel -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>
    <script>
      $(function(){
        'use strict';

        // Inline editor
        var editor = new MediumEditor('.editable');

        // Summernote editor
        $('#editor').summernote({
          height: 550,
          tooltip: false
        })
      });
    </script>
  </body>
</html>
