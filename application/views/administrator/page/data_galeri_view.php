<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>

    <?php require_once(APPPATH .'views/administrator/header.php'); ?>

    <?php require_once(APPPATH .'views/administrator/menu_side.php'); ?>

    <div class="am-mainpanel">
      <div class="am-pagetitle">
        <h5 class="am-title">DATA GALERI</h5>
      </div><!-- am-pagetitle -->

      <div class="am-pagebody">
        <a class="btn btn-primary" href="<?php echo base_url('admin/galeri/input'); ?>" role="button"> <i class="fa fa-plus"></i> TAMBAH GALERI </a>
        <a class="btn btn-primary" href="<?php echo base_url('admin/galeri/categori/input'); ?>" role="button"> <i class="fa fa-plus"></i> TAMBAH CATEGORI GALERI </a>
        <br><br>
        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">Data Galeri</h6>
          <p class="mg-b-20 mg-sm-b-30"></p>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">File</th>
                  <th class="wd-15p">Nama Galeri</th>
                  <th class="wd-15p">Kategori</th>
                  <th class="wd-20p">Tanggal Upload</th>
                  <th class="wd-20p">Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($data_galeri as $dt_galeri): ?>
                  <tr>
                    <td><img style="max-width: 45%;" src="<?php echo base_url('assets_default/img_galeri/'.$dt_galeri->nama_file_galeri); ?>" class="img-responsive img-thumbnail" alt="Image"></td>
                    <td><?php echo $dt_galeri->nama_galeri ?></td>
                    <td><?php echo $dt_galeri->nama_kategori ?></td>
                    <td><?php echo date("d/m/Y", strtotime( $dt_galeri->create_at)); ?></td>
                    <td>
                      <a class="btn btn-primary" data-toggle="modal" href="#modal-id<?php echo $dt_galeri->kode_galeri ?>" role="button">Edit</a>
                      <a class="btn btn-danger" href="<?php echo base_url('administrator/action_delete_galeri/'.$dt_galeri->kode_galeri);  ?>" role="button">Hapus</a>
                    </td>
                  </tr>

                  <div class="modal fade" id="modal-id<?php echo $dt_galeri->kode_galeri ?>">
                    <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content tx-size-sm">
                        <div class="modal-header pd-x-20">
                          <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Edit Data Kerjasama <?php echo $dt_galeri->nama_galeri ?></h6>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body pd-20">
                          <form action="<?php echo base_url('administrator/action_update_galeri'); ?> " method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                            
                            <div class="row mg-t-20">
                              <label class="col-sm-3 form-control-label">Nama galeri : <span class="tx-danger">*</span></label>
                              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" required name="nama_galeri" value="<?php echo $dt_galeri->nama_galeri ?> " placeholder="Nama">
                                <input type="hidden" class="form-control" required name="kode_galeri" value="<?php echo $dt_galeri->kode_galeri ?> " placeholder="Nama">
                              </div>
                            </div><!-- row -->

                            <div class="row mg-t-20">
                              <label class="col-sm-3 form-control-label">Kategori Galeri: <span class="tx-danger">*</span></label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                  <select class="form-control select2" required name="kategori_galeri" data-placeholder="Kategori Galeri">
                                    <option value="">-- Pilih Kategori Galeri --</option>
                                    <?php foreach ($data_categori as $dt_categori): ?>
                                      <option <?php echo $dt_categori->kode_kategori_galeri == $dt_galeri->kode_kategori_galeri ? 'selected = "selected"': ''; ?> value="<?php echo $dt_categori->kode_kategori_galeri ?>"><?php echo $dt_categori->nama_kategori ?></option>
                                    <?php endforeach ?>
                                  </select>
                                </div>
                            </div>

                            <div class="row mg-t-20">
                              <label class="col-sm-3 form-control-label">Upload File: <span class="tx-danger">*</span></label>
                                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                  <label class="custom-file">
                                    <input type="file" id="file2" name="logo_galeri" accept=".png, .jpg, .jpeg" class="custom-file-input">
                                    <input type="hidden" name="img_old" id="input" class="form-control" value="<?php echo $dt_galeri->nama_file_galeri ?> ">
                                    <span class="custom-file-control custom-file-control-primary"></span>
                                </label>
                              </div>
                            </div>

                            <div class="row mg-t-20">
                              <label class="col-sm-3 form-control-label">Link sumber galeri : <span class="tx-danger">*</span></label>
                              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                                <input type="text" class="form-control" required name="link_galeri" value="<?php echo $dt_galeri->link_galeri ?>" placeholder="Link sumber galeri">
                              </div>
                            </div><!-- row -->
                            
                            
                          </div><!-- modal-body -->
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-info pd-x-20">Save changes</button>
                            <button type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Close</button>
                          </div>
                        </form>

                      </div>
                    </div><!-- modal-dialog -->
                  </div><!-- modal -->


                <?php endforeach ?>
               
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->

      </div><!-- am-pagebody -->
      <?php require_once(APPPATH .'views/administrator/footer.php'); ?>
    </div><!-- am-mainpanel -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>
     
  </body>
</html>
