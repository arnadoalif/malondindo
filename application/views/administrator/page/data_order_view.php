<!DOCTYPE html>
<html lang="en">
  <head>

    <?php require_once(APPPATH .'views/include/admin/include_style.php'); ?>
  </head>

  <body>

    <?php require_once(APPPATH .'views/administrator/header.php'); ?>

    <?php require_once(APPPATH .'views/administrator/menu_side.php'); ?>

    <div class="am-mainpanel">
      <div class="am-pagetitle">
        <h5 class="am-title">DATA ORDER</h5>
      </div><!-- am-pagetitle -->

      <div class="am-pagebody">
        
        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">Data Order</h6>
          <p class="mg-b-20 mg-sm-b-30"></p>

          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th>#</th>
                  <th class="wd-15p">Nama Pelanggang</th>
                  <th class="wd-15p">Order Malond</th>
                  <th class="wd-20p">Nomor Telepon</th>
                  <th class="wd-20p">Email Pelanggan</th>
                  <th class="wd-20p">Alamat Pemesan</th>
                  <th class="wd-15p">Jumlah Order</th>
                  <th class="wd-10p">Tanggal Order</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; foreach ($data_order as $dt_order): ?>
                  
                <tr>
                  <td><?php echo $i++ ?> </td>
                  <td><?php echo $dt_order->nama_order ?> </td>
                  <td><?php echo $dt_order->nama_produk_order ?> </td>
                  <td><?php echo $dt_order->nomor_telepon_order ?> </td>
                  <td><?php echo $dt_order->email_order ?> </td>
                  <td><?php echo $dt_order->alamat_pemesan ?> </td>
                  <td><?php echo $dt_order->jumlah_order ?> </td>
                  <td><?php echo date("d/m/Y", strtotime( $dt_order->tanggal_order)); ?></td>
                </tr>

                <?php endforeach ?>

              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->

      </div><!-- am-pagebody -->
      <?php require_once(APPPATH .'views/administrator/footer.php'); ?>
    </div><!-- am-mainpanel -->

    <?php require_once(APPPATH .'views/include/admin/include_script.php'); ?>
  </body>
</html>
