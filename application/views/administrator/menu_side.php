<div class="am-sideleft">
     <!--  <ul class="nav am-sideleft-tab">
        <li class="nav-item">
          <a href="#mainMenu" class="nav-link active"><i class="icon ion-ios-home-outline tx-24"></i></a>
        </li>
        <li class="nav-item">
          <a href="#emailMenu" class="nav-link"><i class="icon ion-ios-email-outline tx-24"></i></a>
        </li>
        <li class="nav-item">
          <a href="#chatMenu" class="nav-link"><i class="icon ion-ios-chatboxes-outline tx-24"></i></a>
        </li>
        <li class="nav-item">
          <a href="#settingMenu" class="nav-link"><i class="icon ion-ios-gear-outline tx-24"></i></a>
        </li>
      </ul> -->

      <div class="tab-content">
        <ul class="nav am-sideleft-menu">
            <li class="nav-item">
              <a href="<?php echo base_url('admin'); ?>" class="nav-link active">
                <i class="icon ion-ios-home-outline"></i>
                <span>Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('admin/resep'); ?>" class="nav-link">
                <i class="icon ion-coffee"></i>
                <span>Resep</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('admin/testimoni'); ?>" class="nav-link">
                <i class="icon ion-chatbubble-working"></i>
                <span>Testimoni</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('admin/kerjasama'); ?>" class="nav-link">
                <i class="fa fa-handshake-o"></i> 
                &nbsp;&nbsp;<span>Kerjasama</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('admin/inbox'); ?>" class="nav-link">
                <i class="icon ion-email"></i>
                <span>Inbox</span>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a href="<?php echo base_url('admin/pelanggan-order'); ?>" class="nav-link">
                <i class="fa fa-cart-arrow-down"></i>
                <span>Daftar Order Malond</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('admin/produk'); ?>" class="nav-link">
                <i class="icon ion-fork"></i>
                <span>Daftar Produk Malond</span>
              </a>
            </li>
             <li class="nav-item">
              <a href="<?php echo base_url('admin/galeri'); ?>" class="nav-link">
                <i class="icon ion-images"></i>
                <span>Galeri Produk Malon</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('admin/testimoni'); ?>" class="nav-link">
                <i class="icon ion-chatbubble-working"></i>
                <span>Testimoni</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url('admin/testimoni_kostumer'); ?>" class="nav-link">
                <i class="icon ion-chatbubble-working"></i>
                <span>Testimoni Kostumer</span>
              </a>
            </li>
            
            
            
            <!-- <li class="nav-item">
              <a href="" class="nav-link with-sub">
                <i class="icon ion-ios-gear-outline"></i>
                <span>Setting</span>
              </a>
              <ul class="nav-sub">
                <li class="nav-item"><a href="<?php echo base_url('admin/sales'); ?>" class="nav-link">Kontak Sales</a></li>
              </ul>
            </li> -->

            <!-- <li class="nav-item">
              <a href="" class="nav-link with-sub">
                <i class="icon ion-ios-gear-outline"></i>
                <span>Forms</span>
              </a>
              <ul class="nav-sub">
                <li class="nav-item"><a href="form-elements.html" class="nav-link">Form Elements</a></li>
                <li class="nav-item"><a href="form-layouts.html" class="nav-link">Form Layouts</a></li>
                <li class="nav-item"><a href="form-validation.html" class="nav-link">Form Validation</a></li>
                <li class="nav-item"><a href="form-wizards.html" class="nav-link">Form Wizards</a></li>
                <li class="nav-item"><a href="form-editor-text.html" class="nav-link">Text Editor</a></li>
              </ul>
            </li> -->

          </ul>
      </div><!-- tab-content -->
    </div><!-- am-sideleft -->