<!doctype html>
<html class="no-js" lang="">

<head>
     <?php require_once(APPPATH .'views/include/front/include_style.php'); ?>
</head>

<body class="pattern-fixed pattern">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  

    <!-- Pre Loader
	============================================ -->
	<!-- <div class="preloader">
		<div class="loading-center">
			<div class="loading-center-absolute">
				<div class="object object_one"></div>
				<div class="object object_two"></div>
				<div class="object object_three"></div>
			</div>
		</div>
	</div> -->
    <!-- Body main wrapper start -->
    <div class="boxed-layout fixed">
        <div class="wrapper white-bg">

        <?php $this->load->view('front_pages/menu_header_view'); ?>

       <!--Header section end-->

        <!--Breadcrubs start-->
        <div class="breadcrubs ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        
                    </div>
                </div>
            </div>
        </div>
        <!--Breadcrubs end-->
        <!--elements start-->
        <div class="elements resep-malond ptb-100">
            <div class="container">
                    <div class="row">
                        <?php foreach ($data_resep as $dt_resep): ?>
                            <div class="col-md-6">
                                <div class="properties-video">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe src="https://www.youtube.com/embed/<?php echo $dt_resep->link_id_resep_youtube ?>" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    <h5 class="mb-30"><a href="<?php echo base_url('resep/detail-resep/'.$dt_resep->slug_resep) ?>" title="">LIHAT RESEP <?php echo strtoupper($dt_resep->nama_resep); ?></a></h5>
                                </div>
                            </div>
                        <?php endforeach ?>
                        
                        
                    </div>
                </div>
        </div>
        <!--elements end-->

            <?php $this->load->view('front_pages/team_sales_view'); ?>
        

            <?php $this->load->view('front_pages/footer_view'); ?>
        </div>


    </div>
    <!-- Body main wrapper end -->
    
    <!-- All js plugins included in this file. -->
    <?php require_once(APPPATH .'views/include/front/include_script.php'); ?>

</body>

</html>