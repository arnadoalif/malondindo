<!doctype html>
<html class="no-js" lang="">

<head>
     <?php require_once(APPPATH .'views/include/front/include_style.php'); ?>
</head>

<body class="pattern-fixed pattern">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  

    <!-- Pre Loader
	============================================ -->
	<!-- <div class="preloader">
		<div class="loading-center">
			<div class="loading-center-absolute">
				<div class="object object_one"></div>
				<div class="object object_two"></div>
				<div class="object object_three"></div>
			</div>
		</div>
	</div> -->
    <!-- Body main wrapper start -->
    <div class="boxed-layout fixed">
        <div class="wrapper white-bg">

        <?php $this->load->view('front_pages/menu_header_view'); ?>

       <!--Header section end-->

            <!--Breadcrubs start-->
            <div class="testimoni ptb-100">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="breadcurbs-inner text-center">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Breadcrubs end-->
            
            <div class="our-blog-pages ptb-80">
                <div class="bg-mg-1">
                    <div class="container">
                       <!--  <div class="row">
                           
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single-blog blog-video mb-30">
                                    <div class="blog-img">
                                        <div class="blog-thumbnail">
                                            <img src="<?php echo base_url('assets/images/blog/2.jpg'); ?>" alt="">
                                        </div>
                                        <div class="blog-hover">
                                            <a href="https://www.youtube.com/embed/oxPgDogVFnQ"><i class="mdi mdi-play"></i></a>    
                                        </div>
                                        <div class="blog-desc">
                                            <div class="publish-date">
                                                <p>13<span>Mar</span></p>
                                            </div>
                                            <div class="blog-title">
                                                <h3><a href="#">CHEF DIMAS <br> YOGYAKARTA</a></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single-blog blog-video mb-30">
                                    <div class="blog-img">
                                        <div class="blog-thumbnail">
                                            <img src="<?php echo base_url('assets/images/blog/2.jpg'); ?>" alt="">
                                        </div>
                                        <div class="blog-hover">
                                            <a href="https://www.youtube.com/embed/oxPgDogVFnQ"><i class="mdi mdi-play"></i></a>    
                                        </div>
                                        <div class="blog-desc">
                                            <div class="publish-date">
                                                <p>13<span>Mar</span></p>
                                            </div>
                                            <div class="blog-title">
                                                <h3><a href="#">RISTA LARA ROSANTI <br> CUPUWATU RESTO</a></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="single-blog blog-video mb-30">
                                    <div class="blog-img">
                                        <div class="blog-thumbnail">
                                            <img src="<?php echo base_url('assets/images/blog/2.jpg'); ?>" alt="">
                                        </div>
                                        <div class="blog-hover">
                                            <a href="https://www.youtube.com/embed/oxPgDogVFnQ"><i class="mdi mdi-play"></i></a>    
                                        </div>
                                        <div class="blog-desc">
                                            <div class="publish-date">
                                                <p>13<span>Mar</span></p>
                                            </div>
                                            <div class="blog-title">
                                                <h3><a href="#">RISTA LARA ROSANTI <br> CUPUWATU GARDEN</a></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div> -->

                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="rumah-makan">
                                    <div class="text-rumahmakan pd-30">
                                        <h3>Rumah Makan & Restoran Dengan Menu Malond</h3>
                                    </div>
                                    <div class="list-rumahmakan">
                                        <ol>
                                            <li>Cupuwatu Resto-Yogya</li>
                                            <li>RM. Hj. Ciganea Group </li>
                                            <li>RM. Pring Asri-Bumiayu</li>
                                            <li>RM. Ayam Qu Group</li>
                                            <li>RM. Ayam Lepaas Group</li>
                                            <li>RM. Sangrai-Bandung</li>
                                            <li>SFA Steak Group</li>
                                            <li>Bale Roso-Yogyakarta</li>
                                            <li>RM. Pring Asri-Bumiayu</li>
                                            <li>RM. Ny Malone-Jakarta </li>
                                            <li>RM. Ampera </li>
                                            <li>RM. Bebek Caleo</li>
                                            <li> Hotel Jayakarta-Yogya</li>
                                            <li>Cupuwatu Garden-Yogya</li>
                                            <li>Restauran di Bali,</li>
                                            <li>Dan masih banyak lagi</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="distribusi">
                                    <div class="img-distribusi">
                                        <img src="<?php echo base_url('assets/images/assets_web_malond/img-ani-malond.png'); ?> " class="img-responsive" alt="Image">
                                    </div>
                                    <div class="list-distribusi">
                                        <h3 class="text-distribusi">Distribusi Malond</h3>
                                        <div class="row">
                                            <div class="col-xs-6 col-md-4 col-lg-4">
                                                <ol>
                                                    <li>Yogyakarta</li>
                                                    <li>Batam</li>
                                                    <li>Palembang</li>
                                                    <li>Lampung</li>
                                                    <li>Jabodetabek</li>
                                                    <li>Bandung</li>
                                                    <li>Purwokerto</li>
                                                    <li>Bumiayu</li>
                                                </ol>
                                            </div>
                                            <div class="col-xs-6 col-md-6 col-lg-6">
                                                <ol start="9">
                                                    <li>Surabaya</li>
                                                    <li>Pangkal Pinang</li>
                                                    <li>Bangka Belitung</li>
                                                    <li>Kalimantan Timur</li>
                                                    <li>Kalimantan Selatan</li>
                                                    <li>Bali</li>
                                                    <li>Makasar</li>
                                                    <li>Kota besar lainnya</li>
                                                </ol>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php $this->load->view('front_pages/team_sales_view'); ?>
        

            <?php $this->load->view('front_pages/footer_view'); ?>
        </div>


    </div>
    <!-- Body main wrapper end -->
    
    <!-- All js plugins included in this file. -->
    <?php require_once(APPPATH .'views/include/front/include_script.php'); ?>

</body>

</html>