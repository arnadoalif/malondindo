﻿<!doctype html>
<html class="no-js" lang="">

<head>
     <?php require_once(APPPATH .'views/include/front/include_style.php'); ?>
</head>

<body class="pattern-fixed pattern">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  

    <!-- Pre Loader
    ============================================ -->
    <!-- <div class="preloader">
        <div class="loading-center">
            <div class="loading-center-absolute">
                <div class="object object_one"></div>
                <div class="object object_two"></div>
                <div class="object object_three"></div>
            </div>
        </div>
    </div> -->
    <!-- Body main wrapper start -->
    <div class="boxed-layout fixed">
        <div class="wrapper white-bg">

        <?php $this->load->view('front_pages/menu_header_view'); ?>

       <!--Header section end-->

       <!-- <div id="carousel-example" class="carousel slide" data-ride="carousel"> -->
            <div class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <!-- <div class="item ">
                      <a href="#"><img src="<?php echo base_url('assets/images/assets_web_malond/lebaran.jpg'); ?>" /></a>
                      <div class="carousel-caption">
                        <h3>Meow</h3>
                        <p>Just Kitten Around</p>
                      </div>
                    </div> -->
                    <div class="item active">
                      <a href="#"><img src="<?php echo base_url('assets/images/assets_web_malond/backround_slide.png'); ?>" /></a>
                      <div class="carousel-caption">
                        <!-- <h3>Meow</h3>
                        <p>Just Kitten Around</p> -->
                      </div>
                    </div>
                </div>
            </div>

            <div class="penjelasan">
                 <div class="container-fluid" >
                        <div class="row">
                            
                            <div class="col-md-4 col-lg-4 aos-item" data-aos="fade-up">
                                <div class="offer-inner text-center">
                                    <img src="<?php echo base_url('assets/images/assets_web_malond/icon_halal.png'); ?>" alt="">
                                    <h3>HALAL</h3>
                                    <p class="text-penjelasan">Rumah Pemotongan Burung (RPB) 
                                        Kami Telah Bersertifikasi Halal MUI &
                                        Diproses Secara Modern.</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 aos-item" data-aos="fade-up">
                                <div class="offer-inner text-center">
                                    <img src="<?php echo base_url('assets/images/assets_web_malond/icon_mudah_masak.png'); ?>" alt="">
                                    <h3>MUDAH DIOLAH</h3>
                                    <p class="text-penjelasan">Tekstur Dagingnya Empuk & Lembut,
                                        Sehingga Bumbu Masak 
                                        Mudah Meresap Ke Dalam Daging</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 aos-item">
                                <div class="offer-inner text-center" data-aos="fade-up">
                                    <img src="<?php echo base_url('assets/images/assets_web_malond/icon_gizi_tinggi.png'); ?>" alt="">
                                    <h3>GIZI TINGGI</h3>
                                    <p class="text-penjelasan">
                                        Tinggi akan Protein, Zat Besi, Kalsium & 
                                        Fosfor, Serta Kandungan Kolesterol Rendah
                                        dibandingkan Daging Bebek.</p>
                                </div>
                            </div>
    
                        </div>
                    </div>
            </div>

            <!--Home about section start-->
            <div class="home-about img-bg ptb-210">
                <div class="container">
                    <div class="row">

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="welcome-about" >
                                <h2 class="text_helper_normal">APA ITU MALOND ? </h2>
                                <h3 class="title_2">
                                    Malond adalah produk komoditas dari PT. Malond Indo 
                                    Perkasa sejak tahun 2010. 
                                </h3>
                                <h3 class="title_2">
                                    Mengapa kami menamakan produk kami Malond? 
                                    Malond adalah singkatan dari “Manuk Londo” yang dalam 
                                    Bahasa Indonesia berarti Burung Bule, karena Malond 
                                    adalah hasil persilangan unggas burung ASLI yang berasal 
                                    dari Negara Perancis.
                                </h3>
                                <div class="read-more" >
                                    <a class="btn btn-read btn-info" href="<?php echo base_url('profil') ?>" role="button">Baca Selengkapnya <i class="fa fa-play"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="welcome-about-img">
                                <!-- <img class="img-malondcs" src="images/new_assets/gambar%204.png" alt=""> -->
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <!--Home about section end-->

            <!--popular dises start-->
            <div class="popular-dishes">
                <div class="ptb-50">
                    <div class="container">
                        <div id="products" class="row list-group">
                            
                            <div class="item  col-xs-12 col-lg-4">
                                <div class="thumbnail">
                                    
                                    <div class="nama">
                                        <h4 class="group inner list-group-item-heading">PRODUK MALOND</h4>
                                        <p>Kami Sediakan 5 Pilihan Grade Malond Sesuai Kebutuhan Anda</p>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="item  col-xs-12 col-lg-4">
                                <div class="thumbnail">
                                    <img class="group list-group-image" src="<?php echo base_url('assets/images/new_assets/malond%201.png'); ?>" alt="" />
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">
                                            MALOND GRADE RESTO</h4>
                                        <p class="group inner list-group-item-text">Berat Min : 225 Gram <br> Isi/Pack : 5 Ekor</p>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="item  col-xs-12 col-lg-4">
                                <div class="thumbnail">
                                    <img class="group list-group-image" src="<?php echo base_url('assets/images/new_assets/malond%202.png'); ?>" alt="" />
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">
                                            MALOND GRADE A</h4>
                                        <p class="group inner list-group-item-text">Berat Min : 205 - 224 Gram <br> Isi/Pack : 5 Ekor</p>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="item  col-xs-12 col-lg-4">
                                <div class="thumbnail">
                                    <img class="group list-group-image" src="<?php echo base_url('assets/images/new_assets/malond%203.png'); ?>" alt="" />
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">
                                            MALOND GRADE B</h4>
                                        <p class="group inner list-group-item-text">Berat Min : 185 - 204 Gram <br> Isi/Pack : 5 Ekor</p>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="item  col-xs-12 col-lg-4">
                                <div class="thumbnail">
                                    <img class="group list-group-image" src="<?php echo base_url('assets/images/new_assets/malond%204.png'); ?>" alt="" />
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">
                                            MALOND GRADE C</h4>
                                        <p class="group inner list-group-item-text">Berat Min : 165 - 184 Gram <br> Isi/Pack : 5 Ekor</p>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="item  col-xs-12 col-lg-4">
                                <div class="thumbnail">
                                    <img class="group list-group-image" src="<?php echo base_url('assets/images/new_assets/malond%205.png'); ?>" alt="" />
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">
                                            MALOND GRADE D</h4>
                                        <p class="group inner list-group-item-text">Berat Min : 142 - 164 Gram <br> Isi/Pack : 10 Ekor</p>
                                        
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--popular dises end-->

            <!--Team brand start-->
            <div class="team-brand">
                <div class="container">
                    <div class="col-md-12 col-lg-12">
                        <div class="section-title white_bg mb-20 text-center">
                            <p class="text_helper_normal">TESTIMONI PELANGGAN KAMI</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="container">
                           
                            <!-- Carousel Start below -->
                            <div id="carousel1" class="carousel slide testimonial-style" data-ride="carousel">
                                <!-- Indicators -->
                                <!-- <ol class="carousel-indicators">
                                    <?php $i=0; foreach ($data_testimoni as $dt_num_testimoni): ?>
                                        <?php if ($i == 0): ?>
                                            <li data-target="#carousel1" data-slide-to="<?php echo $i ?>" class="active"></li>
                                        <?php else: ?>
                                            <li data-target="#carousel1" data-slide-to="<?php echo $i ?>"></li>
                                        <?php endif ?>
                                    <?php $i++; endforeach ?>
                                </ol> -->
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php $i=0; foreach ($data_testimoni as $dt_testimoni): ?>
                                        <?php if ($i == 0): ?>
                                            <div class="item active">
                                                <div class="carousel-caption testimonial-caption-style">
                                                  <img src="<?php echo base_url('assets_default/img_testimoni/'.$dt_testimoni->nama_file) ?>" alt="Testimoni">
                                                  <p class="text-testimoni"><?php echo $dt_testimoni->pesan_testimoni ?></p>
                                                  <p class="testimonial-authors"><?php echo $dt_testimoni->nama_testimoni ?></p>
                                                </div>
                                            </div>
                                        <?php else: ?>
                                            <div class="item">
                                                <div class="carousel-caption testimonial-caption-style">
                                                  <img src="<?php echo base_url('assets_default/img_testimoni/'.$dt_testimoni->nama_file) ?>" alt="Testimoni">
                                                  <p class="text-testimoni"><?php echo $dt_testimoni->pesan_testimoni ?></p>
                                                  <p class="testimonial-authors"><?php echo $dt_testimoni->nama_testimoni ?></p>
                                                </div>
                                            </div>
                                        <?php endif ?>
                                        

                                    <?php $i++; endforeach ?>
                                </div>

                                <!-- Controls -->
                                <a class="left carousel-control" href="#carousel1" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> </a>
                                <a class="right carousel-control" href="#carousel1" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!--Team brand end-->

            <!--Team brand start-->
            <div class="team-brand ptb-10">
                <div class="container">
                    <div class="col-md-12 col-lg-12">
                        <div class="section-title white_bg mb-20 text-center">
                            <p class="text_helper_normal">RUMAH MAKAN PELANGGAN KAMI</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-xs-12 col-sm-12">
                                    <div class="team-list">
                                        <?php foreach ($data_kerjasama as $dt_kerjasama): ?>
                                            
                                            <div class="single-team img-client">
                                                <?php if (!empty($dt_kerjasama->link_partner)): ?>
                                                    <a target="_blank" href="<?php echo $dt_kerjasama->link_partner; ?>"><img class="img-rounded img-responsive" src="<?php echo base_url('assets_default/img_kerjasama/'.$dt_kerjasama->logo_name_partner); ?>" alt=""></a>
                                                <?php else: ?>
                                                    <a href="#"><img class="img-rounded img-responsive" src="<?php echo base_url('assets_default/img_kerjasama/'.$dt_kerjasama->logo_name_partner); ?>" alt=""></a>
                                                <?php endif ?>
                                                
                                            </div>

                                        <?php endforeach ?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Team brand end-->

            <?php $this->load->view('front_pages/team_sales_view'); ?>
        

            <?php $this->load->view('front_pages/footer_view'); ?>
        </div>


    </div>
    <!-- Body main wrapper end -->
    
    <!-- All js plugins included in this file. -->
    <?php require_once(APPPATH .'views/include/front/include_script.php'); ?>

</body>

</html>