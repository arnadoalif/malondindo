<!doctype html>
<html class="no-js" lang="">

<head>
     <?php require_once(APPPATH .'views/include/front/include_style.php'); ?>
</head>

<body class="pattern-fixed pattern">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  

    <!-- Pre Loader
	============================================ -->
	<!-- <div class="preloader">
		<div class="loading-center">
			<div class="loading-center-absolute">
				<div class="object object_one"></div>
				<div class="object object_two"></div>
				<div class="object object_three"></div>
			</div>
		</div>
	</div> -->
    <!-- Body main wrapper start -->
    <div class="boxed-layout fixed">
        <div class="wrapper white-bg">

        <?php $this->load->view('front_pages/menu_header_view'); ?>

       <!--Header section end-->

        <!--Breadcrubs start-->
        <div class="kontak ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        
                    </div>
                </div>
            </div>
        </div>
        <!--Breadcrubs end-->

            <!--contact us pages start-->
        <div class="contact-us">
            <!--Contact bottom section-->
            <div class="contact-bottom-section ptb-100">
                <div class="bg-img"></div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12 contact-form-div">
                            <div class="contact-form">
                                <div class="contact-form-title">
                                    <h2>Kontak Pesan</h2>
                                </div>
                                
                                <?php if (isset($_SESSION['message_data'])): ?>
                                    <div class="alert alert-success" role="alert">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                      <?php echo $_SESSION['message_data'] ?>
                                    </div>
                                <?php endif ?>

                                <?php if (isset($_SESSION['error_data'])): ?>
                                    <div class="alert alert-danger" role="alert">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                      <?php echo $_SESSION['error_data'] ?>
                                    </div>
                                <?php endif ?>

                                <div class="contact-form-box">
                                    
                                    <form action="<?php echo base_url('frontpage/action_inbox'); ?>" method="post">
                                        <input name="nama_inbox" type="text" placeholder="Nama" required>
                                        <input name="nomor_telepon" type="text" placeholder="Nomor Telepon" onkeypress="return isNumberKey(event);" maxlength="13">
                                        <input name="email_inbox" type="text" placeholder="Email" required>
                                        <textarea name="isi_inbox" placeholder="Tuliskan pesan anda" required></textarea>
                                        <button type="submit">Kirim Ke Pihak Malond Indo</button>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 map-div">
                            <div id="contact-map" class="map-area">
                                <div id="googleMap" style="width:100%;height:480px;"></div>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
            <!--Contact bottom section end-->
            
        </div>
       <!--contact us pages end-->

            <?php $this->load->view('front_pages/team_sales_view'); ?>
        

            <?php $this->load->view('front_pages/footer_view'); ?>
        </div>


    </div>
    <!-- Body main wrapper end -->
    
    <!-- All js plugins included in this file. -->
    <?php require_once(APPPATH .'views/include/front/include_script.php'); ?>
    <!-- Map js code here -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAdWLY_Y6FL7QGW5vcO3zajUEsrKfQPNzI"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="<?php echo base_url('assets/js/map.js'); ?>"></script>

</body>

</html>