<!doctype html>
<html class="no-js" lang="">

<head>
     <?php require_once(APPPATH .'views/include/front/include_style.php'); ?>
</head>

<body class="pattern-fixed pattern">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  

    <!-- Pre Loader
	============================================ -->
	<!-- <div class="preloader">
		<div class="loading-center">
			<div class="loading-center-absolute">
				<div class="object object_one"></div>
				<div class="object object_two"></div>
				<div class="object object_three"></div>
			</div>
		</div>
	</div> -->
    <!-- Body main wrapper start -->
    <div class="boxed-layout fixed">
        <div class="wrapper white-bg">

        <?php $this->load->view('front_pages/menu_header_view'); ?>

       <!--Header section end-->

        <!--Home about section start-->
        <div class="home-about white-bg ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="welcome-about-img">
                            <img src="<?php echo base_url('assets/images/assets_web_malond/profil_malond.png'); ?> " alt="">
                        </div>
                        
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="welcome-about">
                            <h3 class="title_2">
                                 PT. Malond Indo Perkasa Adalah Perusahaan Yang Bergerak Dibidang Perdagangan Daging ( Meat Trading Companies) Dengan Komoditas Utama  Daging Malond, Yakni Daging Dari Burung Yang Berasal Dari Negara Perancis.
                                Kantor Kami Berada Di Karawang, Jawa Barat & Yogyakarta, Sejak Berdiri Di  Tahun 2010, Kami Telah Menyuplai Daging Malond Ke Seluruh Indonesia,  Khususnya Di Jawa, Sumatra, Bali, Sulawesi, Kalimantan, Dll.
                                Untuk Mencukupi Kebutuhan Malond, Rumah Potong Burung (RPB) Kami Telah Memakai Peralatan Modern Untuk Menjamin Kualitas Daging Yang Dihasilkan. RPB Kami Juga Telah Memperoleh Sertifikat Halal Dari Majelis Ulama Indonesia D.i. Yogyakarta No: MUI-DIY-1-232-000468-12-16. 
                            </h3>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Home about section end-->

       <!--Team brand start-->
        <div class="team-brand ptb-10">
            <div class="container">
                <div class="col-md-12 col-lg-12">
                    <div class="section-title white_bg mb-20 text-center">
                        <p class="text_helper_normal">RUMAH MAKAN PELANGGAN KAMI</p>
                    </div>
                </div>
                <div class="row">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-xs-12 col-sm-12">
                                <div class="team-list">
                                    <?php foreach ($data_kerjasama as $dt_kerjasama): ?>
                                        
                                        <div class="single-team img-client">
                                            <?php if (!empty($dt_kerjasama->link_partner)): ?>
                                                <a target="_blank" href="<?php echo $dt_kerjasama->link_partner; ?>"><img class="img-rounded img-responsive" src="<?php echo base_url('assets_default/img_kerjasama/'.$dt_kerjasama->logo_name_partner); ?>" alt=""></a>
                                            <?php else: ?>
                                                <a href="#"><img class="img-rounded img-responsive" src="<?php echo base_url('assets_default/img_kerjasama/'.$dt_kerjasama->logo_name_partner); ?>" alt=""></a>
                                            <?php endif ?>
                                            
                                        </div>

                                    <?php endforeach ?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Team brand end-->

            <?php $this->load->view('front_pages/team_sales_view'); ?>
        

            <?php $this->load->view('front_pages/footer_view'); ?>


    </div>
    <!-- Body main wrapper end -->
    
    <!-- All js plugins included in this file. -->
    <?php require_once(APPPATH .'views/include/front/include_script.php'); ?>

</body>

</html>