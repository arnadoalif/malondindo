﻿<!doctype html>
<html class="no-js" lang="">

<head>
     <?php require_once(APPPATH .'views/include/front/include_style.php'); ?>
</head>

<body class="pattern-fixed pattern">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  

    <!-- Pre Loader
	============================================ -->
	<!-- <div class="preloader">
		<div class="loading-center">
			<div class="loading-center-absolute">
				<div class="object object_one"></div>
				<div class="object object_two"></div>
				<div class="object object_three"></div>
			</div>
		</div>
	</div> -->
    <!-- Body main wrapper start -->
    <div class="boxed-layout fixed">
        <div class="wrapper white-bg">

        <?php $this->load->view('front_pages/menu_header_view'); ?>

       <!--Header section end-->

            <!--popular dises start-->
            <div class="popular-dishes">
                <div class="ptb-50">
                    <div class="container">
                        <div id="products" class="row list-group">
                            
                            <div class="item  col-xs-12 col-lg-4">
                                <div class="thumbnail">
                                    
                                    <div class="nama">
                                        <h4 class="group inner list-group-item-heading">PRODUK MALOND</h4>
                                        <p>Kami Sediakan 5 Pilihan Grade Malond Sesuai Kebutuhan Anda</p>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="item  col-xs-12 col-lg-4">
                                <div class="thumbnail">
                                    <img class="group list-group-image" src="<?php echo base_url('assets/images/new_assets/malond%201.png'); ?>" alt="" />
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">
                                            MALOND GRADE RESTO</h4>
                                        <p class="group inner list-group-item-text">Berat Min : 225 Gram <br> Isi/Pack : 5 Ekor</p>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="item  col-xs-12 col-lg-4">
                                <div class="thumbnail">
                                    <img class="group list-group-image" src="<?php echo base_url('assets/images/new_assets/malond%202.png'); ?>" alt="" />
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">
                                            MALOND GRADE A</h4>
                                        <p class="group inner list-group-item-text">Berat Min : 205 - 224 Gram <br> Isi/Pack : 5 Ekor</p>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="item  col-xs-12 col-lg-4">
                                <div class="thumbnail">
                                    <img class="group list-group-image" src="<?php echo base_url('assets/images/new_assets/malond%203.png'); ?>" alt="" />
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">
                                            MALOND GRADE B</h4>
                                        <p class="group inner list-group-item-text">Berat Min : 185 - 204 Gram <br> Isi/Pack : 5 Ekor</p>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="item  col-xs-12 col-lg-4">
                                <div class="thumbnail">
                                    <img class="group list-group-image" src="<?php echo base_url('assets/images/new_assets/malond%204.png'); ?>" alt="" />
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">
                                            MALOND GRADE C</h4>
                                        <p class="group inner list-group-item-text">Berat Min : 165 - 184 Gram <br> Isi/Pack : 5 Ekor</p>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="item  col-xs-12 col-lg-4">
                                <div class="thumbnail">
                                    <img class="group list-group-image" src="<?php echo base_url('assets/images/new_assets/malond%205.png'); ?>" alt="" />
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">
                                            MALOND GRADE D</h4>
                                        <p class="group inner list-group-item-text">Berat Min : 142 - 164 Gram <br> Isi/Pack : 10 Ekor</p>
                                        
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--popular dises end-->

            <div class="keunggulan-produk ptb-50">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-lg-4 pd-3 brown-bg">
                            <div class="keunggulan-malon yellow-color text-center">
                                <h3>KEUNGGULAN</h3>
                                <p class="text-keunggulan">DAGING MALOND</p>
                            </div>
                            <div class="daging-malond">
                                <img src="<?php echo base_url('assets/images/assets_web_malond/gambar%201.png'); ?> " alt="">
                            </div>
                            
                        </div>
                        <div class="col-md-8 col-lg-8">
                            <div class="list-keunggulan">
                                <div class="accordion style1">
                                     <div class="panel-group" id="accordion_1">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1" aria-expanded="true">
                                                     Keunggulan Daging Malond
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_1" class="panel-collapse collapse in" >
                                                <div class="panel-body">
                                                <ol>
                                                    <li>Tekstur daging Lembut, Empuk dan Lezat.</li>
                                                    <li>Bergizi, kaya protein, kaya mikronutrien dan berbagai </li>
                                                    <li>vitamin termasuk Folat, Bit. B Kompleks, Vit. E dan Vit. K.</li>
                                                    <li>Membantu dalam proses pertumbuhan anak.</li>
                                                    <li>Menambah stamina tubuh, kekuatan dan daya ingat.</li>
                                                    <li>Melancarkan peredaran darah dan menguatkan tulang belakang.</li>
                                                    <li>Proses penyembelihan dilakukan di RPB (Rumah Potong Burung) secara islami dan halal (MUI-DIY-1-232-000468-12-16).</li>
                                                    <li>Proses pencabutan bulu, pembersihan dan penyimpanan daging dilakukan secara modern.</li>
                                                    <li>Penyimpanan daging dilakukan di dalam cool storage suhu -18 C dan kontrol yang ketat agar didapatkan produk yang bersih dengan higienis tampa bahan pengawet.</li>
                                                    <li>
                                                        Bebas dari hormon dan antibiotik dosis tinggi karena pakan 
                                                        Malond berbeda dari pakan Ayam Broiler
                                                        Memiliki kandungan koresterol yang sangat rendah
                                                        (hasil uji pusat studi pangandangizi Universitas Gadjah 
                                                        Mada No : PS/396/XII/2016).
                                                    </li>
                                                </ol>
                                                <div>
                                                    <h3>Malond Hidup</h3>
                                                    <img class="pull-right" style="max-width: 45%;" src="<?php echo base_url('assets/images/assets_web_malond/malond%20hidup.png'); ?>" alt="">
                                                </div>
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="sertifikat-produk ptb-50"> 
               <div class="container-fluid">
                   <div class="row">
                       <div class="col-md-6 col-lg-6 pm-0">
                           <div class="mui pd-3">
                               <div class="title-sertifikasi"> 
                                   <img src="<?php echo base_url('assets/images/assets_web_malond/logo_mui.png'); ?>" alt="">
                                   <p class="title-mui">SERTIFIKASI HALAL <br> MUI D.I.YOGYAKARTA</p>
                               </div>
                               <div class="img-sertifikasi">
                                   <img src="<?php echo base_url('assets/images/assets_web_malond/sertifikat_halal.png'); ?>" class="img-responsive" alt="Image">
                               </div>
                           </div>
                       </div>
                       <div class="col-md-6 col-lg-6 pm-0">
                            <div class="ugm pd-3">
                                <div class="title-sertifikasi"> 
                                   <img src="<?php echo base_url('assets/images/assets_web_malond/logo_ugm.png'); ?>" alt="">
                                   <p class="title-ugm">
                                       LAPORAN HASIL UJI <br>
                                        PUSAT STUDI PANGAN DAN GIZI<br>
                                        UNIVERSITAS GAJAH MADA<br>
                                        No : PS/333/VIII/2017<br>
                                   </p>
                               </div>
                               <div class="img-sertifikasi">
                                   <img src="<?php echo base_url('assets/images/assets_web_malond/tabel%20sample.png'); ?>" class="img-responsive max-w-99" alt="Image">
                               </div>
                               <div class="box-kandungan pd-5">
                                   <div class="panel panel-default">
                                       <div class="panel-body">
                                           “Kandungan Daging Malond
                                            Terbukti Sehat & Aman
                                            Di Konsumsi Setiap Hari”
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
            </div>

            <!--Team brand start-->
            <div class="team-brand ptb-10">
                <div class="container">
                    <div class="col-md-12 col-lg-12">
                        <div class="section-title white_bg mb-20 text-center">
                            <p class="text_helper_normal">RUMAH MAKAN PELANGGAN KAMI</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-xs-12 col-sm-12">
                                    <div class="team-list">
                                        <?php foreach ($data_kerjasama as $dt_kerjasama): ?>
                                            
                                            <div class="single-team img-client">
                                                <?php if (!empty($dt_kerjasama->link_partner)): ?>
                                                    <a target="_blank" href="<?php echo $dt_kerjasama->link_partner; ?>"><img class="img-rounded img-responsive" src="<?php echo base_url('assets_default/img_kerjasama/'.$dt_kerjasama->logo_name_partner); ?>" alt=""></a>
                                                <?php else: ?>
                                                    <a href="#"><img class="img-rounded img-responsive" src="<?php echo base_url('assets_default/img_kerjasama/'.$dt_kerjasama->logo_name_partner); ?>" alt=""></a>
                                                <?php endif ?>
                                                
                                            </div>

                                        <?php endforeach ?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Team brand end-->

            <?php $this->load->view('front_pages/team_sales_view'); ?>
        

            <?php $this->load->view('front_pages/footer_view'); ?>
        </div>


    </div>
    <!-- Body main wrapper end -->
    
    <!-- All js plugins included in this file. -->
    <?php require_once(APPPATH .'views/include/front/include_script.php'); ?>

</body>

</html>