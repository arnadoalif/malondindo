<!--Footer section start-->
<div class="footer brown-bg">
    <div class="footer-top ptb-40 brown-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-footer">
                        <h3 class="single-footer-title">KANTOR PUSAT</h3>
                        <div class="single-footer-details mt-30">
                            <p class="addresses">
                                <strong>Komplek Pergudangan Karawang Asri B2 Jl. Surotokunto RW 03 / RW 07 Karawang Timur, Jawa Barat</strong>
                            </p>
                            <p class="email">
                                <strong> Email:</strong> malondindo@gmail.com
                            </p>
                            <p class="phon">
                                <strong>www.malondindo.com</strong>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-footer">
                        <h3 class="single-footer-title">KANTOR CABANG</h3>
                        <div class="single-footer-details mt-30">
                            <p>Jl. Raya Cangkringan Km 5, Ngasem Selomartani, Kalasan, Sleman, DIY</p>
                            <div class="open-list">
                                <p class="addresses">Telepon :</p>
                                <br>
                                <p class="font-30"><strong>0274 - 2850 254</strong></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-footer">
                        <h3 class="single-footer-title">Jam Kerja</h3>
                        <div class="single-footer-details mt-30">
                            <p>PT Malond Indo Perkasa</p>
                            <div class="open-list">
                                <ul>
                                    <li>Senin - Jum'at &nbsp;&nbsp;&nbsp;<span>9:00 AM - 16:00 PM</span></li>
                                    <li>Sabtu &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>9:00 AM - 14:00 PM</span></li>
                                    <li>Minggu &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Tutup</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="single-footer newsletter">
                        <h3 class="single-footer-title">Produk & Distribusi</h3>
                        <div class="single-footer-details mt-30">
                            <p>
                                Khusus menangani penjualan malond untuk menjamin ketersediaan pasokan malond di pasar. Untuk memenuhi permintaan tersebut didirikan Depo Malond dan bekerjasama dengan rumah makan di beberapa kota besar di Pulau Jawa, Sumatera, Bali, Sulawesi dan Kalimantan.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright text-center ptb-20 blue-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <p>Copyright 2018 &copy;. All Rights Reserved. PT MALOND INDO PERKASA</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Footer section end-->