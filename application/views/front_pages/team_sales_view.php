<div class="our-team-area ptb-40" id="hubungi_langsung">
    <div class="container-fluid" >
        <div class="row">
            
            <div class="col-md-4 col-lg-4">
                <div class="section-title white_bg mb-50 text-center">
                    <p class="text_helper">Hubungi Sales Marketing Kami, <br>Untuk Mendapatkan Informasi Detail:</p>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="col-xs-12 col-sm-3" >
                    <img src="<?php echo base_url('assets/images/new_assets/profil%201.png'); ?>" alt="Tami" class="img-responsive img-circle img-contac" />
                </div>
                <div class="col-xs-12 col-sm-9" >
                    <div class="contac-person">
                        <span class="name">Sales Marketing</span><br/>
                        <span class="name">Tami 0811-2959-512</span><br/>
                        <span class="name">
                            <a class="btn hidden-lg btn-info btn-contac-red" href="tel:08112959512" role="button"><i class="fa fa-phone"></i> Telepon</a>
                            <a class="btn btn-success btn-contac-green" target="_blank" href="https://api.whatsapp.com/send?phone=628112959512&text=Saya%20Mau%20Order%25Malond" role="button"><i class="fa fa-whatsapp"></i> Whatsapp</a>
                        </span><br/>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="col-xs-12 col-sm-3" >
                    <img src="<?php echo base_url('assets/images/new_assets/default_cs.gif'); ?>" alt="Ani" class="img-responsive img-circle img-contac" />
                </div>
                <div class="col-xs-12 col-sm-9" >
                    <div class="contac-person">
                        <span class="name">Customer Service</span><br/>
                        <span class="name">0274-285-0254</span><br/>
                        <span class="name">
                            <a class="btn hidden-lg btn-info btn-contac-red" href="tel:02742850254" role="button"><i class="fa fa-phone"></i> Telepon</a>
                        </span><br/>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>