<!--Header section start-->
    	<div class="header sticky-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-3 col-xs-6">
                    <div class="logo"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/images/assets_web_malond/logo_malond.png'); ?> " alt=""></a></div>
                </div>
                <div class="col-md-10 col-sm-9 col-xs-6">
                    <div class="mgea-full-width">
                        <div class="header-right">
                            <div class="header-menu hidden-sm hidden-xs">
                                <div class="menu">
                                    <ul>
                                        <li><a href="<?php echo base_url(); ?>">BERANDA</a></li>
                                        <li><a href="<?php echo base_url('profil'); ?>">PROFIL</a></li>
                                        <li><a href="<?php echo base_url('produk'); ?>">PRODUK</a></li>
                                        <li><a href="<?php echo base_url('testimoni'); ?>">TESTIMONIAL</a></li>
                                        <li><a href="<?php echo base_url('resep'); ?>">RESEP</a></li>
                                        <li><a href="<?php echo base_url('kontak'); ?>">KONTAK</a></li>
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- Mobile menu start -->
        <div class="mobile-menu-area hidden-lg hidden-md">
            <div class="container">
                <div class="col-md-12">
                    <nav id="dropdown">
                        <ul>
                            <li><a href="<?php echo base_url(); ?>">BERANDA</a></li>
                            <li><a href="<?php echo base_url('profil'); ?>">PROFIL</a></li>
                            <li><a href="<?php echo base_url('produk'); ?>">PRODUK</a></li>
                            <li><a href="<?php echo base_url('testimoni'); ?>">TESTIMONIAL</a></li>
                            <li><a href="<?php echo base_url('resep'); ?>">RESEP</a></li>
                            <li><a href="<?php echo base_url('kontak'); ?>">KONTAK</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- Mobile menu end -->
    	</div>