<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>PT. MALOND INDO PERKASA</title>
<link rel="icon" href="<?php echo base_url('assets_default/logo-malon-clr-bg.png'); ?>">
<!-- vendor css -->
<link href="<?php echo base_url('assets_admin/lib/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets_admin/lib/Ionicons/css/ionicons.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets_admin/lib/perfect-scrollbar/css/perfect-scrollbar.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets_admin/lib/jquery-toggles/toggles-full.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets_admin/lib/highlightjs/github.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets_admin/lib/datatables/jquery.dataTables.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets_admin/lib/select2/css/select2.min.css'); ?>" rel="stylesheet">

<link href="<?php echo base_url('assets_admin/lib/medium-editor/medium-editor.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets_admin/lib/medium-editor/default.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets_admin/lib/summernote/summernote-bs4.css'); ?>" rel="stylesheet">

<!-- Amanda CSS -->
<link rel="stylesheet" href="<?php echo base_url('assets_admin/css/amanda.css'); ?>">