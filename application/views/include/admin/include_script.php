<script src="<?php echo base_url('assets_admin/lib/jquery/jquery.js'); ?>"></script>
<script src="<?php echo base_url('assets_admin/lib/popper.js/popper.js'); ?>"></script>
<script src="<?php echo base_url('assets_admin/lib/bootstrap/bootstrap.js'); ?>"></script>
<script src="<?php echo base_url('assets_admin/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js'); ?>"></script>
<script src="<?php echo base_url('assets_admin/lib/jquery-toggles/toggles.min.js'); ?>"></script>

<script src="<?php echo base_url('assets_admin/lib/highlightjs/highlight.pack.js'); ?>"></script>
<script src="<?php echo base_url('assets_admin/lib/datatables/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url('assets_admin/lib/datatables-responsive/dataTables.responsive.js'); ?>"></script>
<script src="<?php echo base_url('assets_admin/lib/select2/js/select2.min.js'); ?>"></script>

<script src="<?php echo base_url('assets_admin/lib/medium-editor/medium-editor.js'); ?>"></script>
<script src="<?php echo base_url('assets_admin/lib/summernote/summernote-bs4.min.js'); ?>"></script>

<script>
      $(function(){
        'use strict';

        // Inline editor
        var editor = new MediumEditor('.editable');

        // Summernote editor
        $('#summernote').summernote({
          height: 550,
          tooltip: false
        })
      });
    </script>

<script>
      $(function(){
        'use strict';

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });

      });
    </script>

<script src="<?php echo base_url('assets_admin/js/amanda.js'); ?>"></script>