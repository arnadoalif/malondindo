<!-- All js plugins included in this file. -->
<script src="<?php echo base_url('assets/js/vendor/jquery-1.12.0.min.js'); ?> "></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?> "></script>
<script src="<?php echo base_url('assets/js/jquery.nivo.slider.pack.js'); ?> "></script>
<script src="<?php echo base_url('assets/js/isotope.pkgd.min.js'); ?> "></script>
<script src="<?php echo base_url('assets/js/ajax-mail.js'); ?> "></script>
<script src="<?php echo base_url('assets/js/jquery.magnific-popup.js'); ?> "></script>
<script src="<?php echo base_url('assets/js/jquery.counterup.min.js'); ?> "></script>
<script src="<?php echo base_url('assets/js/animated-headlines.js'); ?> "></script>
<script src="<?php echo base_url('assets/js/waypoints.min.js'); ?> "></script>
<script src="<?php echo base_url('assets/js/jquery.collapse.js'); ?> "></script>
<script src="<?php echo base_url('assets/js/plugins.js'); ?> "></script>
<script src="<?php echo base_url('assets/js/main.js'); ?> "></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115465825-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-115465825-1');
</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/58e60082f7bbaa72709c4823/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->