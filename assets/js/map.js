// function initialize() {
//     var mapOptions = {
//         zoom: 18,
//         scrollwheel: false,
//         center: new google.maps.LatLng(-6.3208369,107.3248645)
//     };
//     var map = new google.maps.Map(document.getElementById('googleMap'),
//           mapOptions);
//     var marker = new google.maps.Marker({
//         icon: './images/icons/map-marker.png',
//         position: map.getCenter(),
//         map: map
//     });
//     var styles = [
//     {
//         "featureType": "landscape",
//         "elementType": "labels",
//         "stylers": [
//             {
//                 "visibility": "off"
//             }
//         ]
//     },
//     {
//         "featureType": "transit",
//         "elementType": "labels",
//         "stylers": [
//             {
//                 "visibility": "off"
//             }
//         ]
//     },
//     {
//         "featureType": "poi",
//         "elementType": "labels",
//         "stylers": [
//             {
//                 "visibility": "off"
//             }
//         ]
//     },
//     {
//         "featureType": "water",
//         "elementType": "labels",
//         "stylers": [
//             {
//                 "visibility": "off"
//             }
//         ]
//     },
//     {
//         "featureType": "road",
//         "elementType": "labels.icon",
//         "stylers": [
//             {
//                 "visibility": "off"
//             }
//         ]
//     },
//     {
//         "stylers": [
//             {
//                 "hue": "#00aaff"
//             },
//             {
//                 "saturation": -100
//             },
//             {
//                 "gamma": 2.15
//             },
//             {
//                 "lightness": 12
//             }
//         ]
//     },
//     {
//         "featureType": "road",
//         "elementType": "labels.text.fill",
//         "stylers": [
//             {
//                 "visibility": "on"
//             },
//             {
//                 "lightness": 24
//             }
//         ]
//     },
//     {
//         "featureType": "road",
//         "elementType": "geometry",
//         "stylers": [
//             {
//                 "lightness": 57
//             }
//         ]
//     }
// ]

//     map.setOptions({styles: styles});


// }
// google.maps.event.addDomListener(window, 'load', initialize);    

//Google Map
    var get_latitude  = -6.3208369;
    var get_longitude = 107.3248645;

    function initialize_google_map() {
        var myLatlng = new google.maps.LatLng(get_latitude, get_longitude);
        var mapOptions = {
            zoom: 18,
            scrollwheel: false,
            center: myLatlng
        };
        var map = new google.maps.Map(document.getElementById('googleMap'), mapOptions);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize_google_map);